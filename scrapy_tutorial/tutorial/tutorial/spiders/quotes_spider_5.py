import scrapy

class QuotesSpider(scrapy.Spider):
    name = 'quotes_5'
    start_urls = [
        'http://quotes.toscrape.com/',
    ]

    def parse(self, response):
        for quote in response.css('div.quote'):
            yield {
                'title' : quote.css('span.text::text').extract_first(),
                'author' : quote.css('small.author::text').extract_first(),
                'tags' : quote.css('div.tags a.tag::text').extract(),
            }
        for href in response.css('li.next a::attr(href)'):
            yield response.follow(href, callback=self.parse)