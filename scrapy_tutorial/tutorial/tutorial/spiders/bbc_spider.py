import scrapy

class BbcNews(scrapy.Spider):
    name = 'news'
    start_urls = [
        'http://www.bbc.com/',
    ]
    def parse(self, response):
        yield {
            'page_title' : response.css('div h1#page-title::text').extract_first(),
            'module_title' : response.css('div#orb-modules div.content h2.module__title span::text').extract(),
        }