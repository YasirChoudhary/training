import scrapy

class LF(scrapy.Spider):
    name = 'jokes'
    start_urls = [
        'http://www.laughfactory.com/jokes/food-jokes'
    ]

    def parse(self, response):
        yield {
            'text' : response.xpath("//div[@class='joke-text']/p/text()").extract_first(),
            'new_text' : response.css('div.joke-text p::text').extract_first(),
        }