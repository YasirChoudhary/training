0) To install scrapy first you have to create virtualenv and activate it 
$ pip install scrapy
$ scrapy startproject project_name

1) To put our spider to work, go to the project’s top level directory and run:
$ scrapy crawl name_of_spider

2) To open a scrapy shell
$ scrapy shell 'http://quotes.toscrape.com

3) To extract a link
response.css('li.next a::attr(href)').extract_first()

4) to save crawled data in json
scrapy crawl name_of_spider -o quotes.json