# File Permissions
 Linux is a clone of UNIX, the multi-user operating system which can be accessed by many users simultaneously. Linux can also be used in mainframes and servers without any modifications. But this raises security concerns as an unsolicited or malign user can corrupt, change or remove crucial data. For effective security, Linux divides authorization into 2 levels.
1. Ownership
1. Permission

## Ownership of Linux files

Every file and directory on your Unix/Linux system is assigned 3 types of owner, given below.

**User**
A user is the owner of the file. By default, the person who created a file becomes its owner. Hence, a user is also sometimes called an owner.

**Group**
A user- group can contain multiple users. All users belonging to a group will have the same access permissions to the file. Suppose you have a project where a number of people require access to a file. Instead of manually assigning permissions to each user, you could add all users to a group, and assign group permission to file such that only this group members and no one else can read or modify the files.

**Other**
Any other user who has access to a file. This person has neither created the file, nor he belongs to a usergroup who could own the file. Practically, it means everybody else.

Now, the big question arises how does Linux distinguish between these three user types so that a user 'A' cannot affect a file which contains some other user 'B's' vital information/data. It is like you do not want your colleague, who works on your Linux computer, to view your images. This is where Permissions set in, and they define user behavior.

## Permissions

Every file and directory in your UNIX/Linux system has following 3 permissions defined for all the 3 owners discussed above.

1. **Read**: This permission give you the authority to open and read a file. Read permission on a directory gives you the ability to lists its content.
2. **Write**: The write permission gives you the authority to modify the contents of a file. The write permission on a directory gives you the authority to add, remove and rename files stored in the directory.
3. **Execute**: In Unix/Linux, you cannot run a program unless the execute permission is set. If the execute permission is not set, you might still be able to see/modify the program code(provided read & write permissions are set), but not run it.

**Run The Below Commands**
```sh
$ ls -l
```
The output will look something like as shown below
```sh
total 1007768
-rwxrwxrwx  1 yasir yasir       1236 Oct 12  2017 assert_node_id.py
drwxr-xr-x  2 yasir yasir       4096 Mar 28 11:27 Desktop
```
The permisions are broken into 4 sections.

```-rwxrwxrwx```
1. ```-``` : indicates a file.
  ```d``` : indicates a directory.
2. ```rwx``` : read write and execute permission for owner of a file.
3.  ```rwx```: read write and execute permission for owner of a file.
4. ```rwx```: read write and execute permission for owner of a file.

**The Character rwx- indicates**
r = read permission
w = write permission
x = execute permission
\- = no permission

### Changing file/directory permissions with 'chmod' command

We can use the 'chmod' command which stands for 'change mode'. Using the command, we can set permissions (read, write, execute) on a file/directory for the owner, group and the world.

**Syntax**
```sh
$ chmod permissions filename
```

#### There are 2 ways to use the command -
1. Symbolic mode
1. Absolute mode

**1. Symbolic mode**
In the symbolic mode, you can modify permissions of a specific owner. It makes use of mathematical symbols to modify the file permissions.

```+``` : Adds a permission to a file or directory
```-``` : Removes the permission
```=``` : Sets the permission and overrides the permissions set earlier.

If you wanted to add or remove permissions to the user, use the command “chmod” with a “+” or “–“, along with the r (read), w (write), x (execute) attribute followed by the name of the directory or file.

chmod +rwx “name of the file”
chmod –rwx “name of the directory”

EX:
```sh
$ chmod -rwx Desktop
```
Here we are removing the read write and execute permission to the Desktop directory from user yasir.

**The various owners are represented as**
```u``` :	user/owner
```g``` :	group
```o``` :	other
```a``` :	all

**2. Absolute(Numeric) Mode**
In this mode, file permissions are not represented as characters but a three-digit octal number.
```Numbers```    : ```Permission Type```
4 : Read
2 : Write
1 : Execute
0 : No Permission

Ex:
```sh
$ chmod 777 workfolder
```
Will give read, write, and execute permissions for User, Group And Other Users too.

### Changing Ownership and Group
For changing the ownership of a file/directory, you can use the following command:

Syntax:
```sh
$ chown user
```

In case you want to change the user as well as group for a file or directory use the command
```sh
$ chown user:group filename
```

In case you want to change group-owner only, use the command
```sh
$ chgrp group_name filename
```

[Reference Link1](https://www.guru99.com/file-permissions.html)
[Reference Link2](https://www.pluralsight.com/blog/it-ops/linux-file-permissions)





