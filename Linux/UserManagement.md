# User Management
Linux is a multi-user operating system, which means that more than one user can use Linux at the same time. Linux provides a beautiful mechanism to manage users in a system. One of the most important roles of a system administrator is to manage the users and groups in a system.

**Linux user**
A user or account of a system is uniquely identified by a numerical number called the UID (unique identification number). There are two types of users – the root or super user and normal users. A root or super user can access all the files, while the normal user has limited access to files. A super user can add, delete and modify a user account. The full account information is stored in the **/etc/passwd** file and a hash password is stored in the file **/etc/shadow**. Some operations on a user account are discussed below.

## Creating a user
A user can be added by running the **useradd** command at the command prompt. After creating the user, set a password using the
passwd utility, as follows:

```sh
$ sudo useradd -d /home/yasir -m -s /bin/bash yasir
```
**Creating the user password**
```sh
$ sudo passwd yasir
```
**Providing the sudo access to the user by using usermod command**
```sh
$ usermod -a -G sudo yasir
```
The system automatically assigns a UID. The useradd command creates a user private group whenever a new user is added to the system and names the group after the user.

## Getting the User ID and Group Information:
To show all the user information and group memberships, we can use the id command:
```sh
$ id yasir
uid=1000(yasir) gid=1000(yas groups=1000(yasir),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),113(lpadmin),128(sambashare)
```
We could also get all the users’ groups with the groups command:
```sh
$ groups yasir
```

### There are two kinds of groups:

**Primary Group**: This is the group applied to you when you log in; in most user cases it has the same name as your login name. The primary group is used by default when creating new files (or directories), modifying files, or executing commands.

**Secondary Groups (AKA Supplementary Groups)**: These are groups you are a member of beyond your primary group. As an example, this means that if a directory or file belongs to the www-data group (as used by the web server process in this case), then all www-data group members can read or modify these files directly (assuming the permissions also allow for this).

A list of all currently available groups can be found in the **/etc/group** file.

Note that every group can also have administrators, members, and a password. See explanations of the gpasswd and sg commands below.

**Creating groups and adding users**
Now it's time to create a group. Let's create the group C-Data. To do this, you would issue the command:
```sh
$  sudo groupadd C-Data
```
If you wish to add a password, then type gpasswd with the group name, as follow:
```sh
$ sudo gpasswd C-Data
```

Now we want to add our new user, yasir, to the group C-Data. For this we will take advantage of the usermod command. This command is quite simple to use.
```sh
$ sudo usermod -a -G C-Data yasir
```
The -a option tells usermod we are appending and the -G option tells usermod we are appending to the group name that follows the option.
How do you know which users are already a member of a group? You can do this the old-fashioned way like so:
```sh
$ grep C-Data /etc/group
```
The above command will list pertinent information about the group
Another method for finding out who is in a group is with the command members. This command isn't installed on most distributions, but can be installed from the standard repositories. If you're using a Ubuntu distribution, the command for installation would be:

```sh
$ sudo apt-get install members
```
Once installed, the command for listing out who is in our editorial group would be:
```sh
$ members C-Data
```
**Removing group password:**
To remove a group password, run gpasswd –r with the relevant group name, as follow:
```sh
$ sudo gpasswd -r C-Data
```
**Changing the group’s name:**
To change the group’s name, run the groupmod command with the -n option as a super user, as shown below:
for example if we want to change group name from C-Data  to www-Data write below commands.
```sh
$ sudo groupmod -n www-Data C-Data
```
**Deleting a group:**
Before deleting a primary group, delete the users of that primary group. To delete a group, run the groupdel command with the group name, as shown below:
```sh
$ sudo groupdel www-Data
```

### Locking and unlocking a user
A super user can lock and unlock a user account. To lock an account, one needs to invoke passwd with the -l option.
The –l option with passwd lock an account, as
shown below:
```sh
$ passwd -l yasir
```
The –u option with passwd unlock an account, as
shown below:
```sh
$ sudo passwd -u yasir
```

**Changing a user name:** 
The –l option with the usermod command changes the login (user) name, as shown below:
```sh
$ sudo usermod -l "chaudhary" yasir
```

**Removing a user:**
Combining userdel with the –r option drop a user and the home directory associated with that user, as shown below:
```sh
$ sudo userdel -r ahmed
```








