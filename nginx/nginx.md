# NGINX

**NGINX** is open source software for web serving, reverse proxying, caching, load balancing, media streaming, and more. It started out as a web server designed for maximum performance and stability. In addition to its HTTP server capabilities, NGINX can also function as a proxy server for email (IMAP, POP3, and SMTP) and a reverse proxy and load balancer for HTTP, TCP, and UDP servers.

# Backstory
Igor Sysoev originally wrote NGINX to solve the C10K problem, a term coined in 1999 to describe the difficulty that existing web servers experienced in handling large numbers (the 10K) of concurrent connections (the C). With its event-driven, asynchronous architecture, NGINX revolutionized how servers operate in high-performance contexts and became the fastest web server available.

After open sourcing the project in 2004 and watching its use grow exponentially, Sysoev co-founded NGINX, Inc. to support continued development of NGINX and to market NGINX Plus as a commercial product with additional features designed for enterprise customers. Today, NGINX and NGINX Plus can handle hundreds of thousands of concurrent connections, and power more than 50% of the busiest sites on the web.

# NGINX as a Web Server

The goal behind NGINX was to create the fastest web server around, and maintaining that excellence is still a central goal of the project. NGINX consistently beats Apache and other servers in benchmarks measuring web server performance. Since the original release of NGINX however, websites have expanded from simple HTML pages to dynamic, multifaceted content. NGINX has grown along with it and now supports all the components of the modern Web, including WebSocket, HTTP/2, and streaming of multiple video formats (HDS, HLS, RTMP, and others).

# NGINX Beyond Web Serving

Though NGINX became famous as the fastest web server, the scalable underlying architecture has proved ideal for many web tasks beyond serving content. Because it can handle a high volume of connections, NGINX is commonly used as a reverse proxy and load balancer to manage incoming traffic and distribute it to slower upstream servers – anything from legacy database servers to microservices.

NGINX also is frequently placed between clients and a second web server, to serve as an SSL/TLS terminator or a web accelerator. Acting as an intermediary, NGINX efficiently handles tasks that might slow down your web server, such as negotiating SSL/TLS or compressing and caching content to improve performance. Dynamic sites, built using anything from Node.js to PHP, commonly deploy NGINX as a content cache and reverse proxy to reduce load on application servers and make the most effective use of the underlying hardware.

# What Can NGINX and NGINX Plus Do for You?

NGINX Plus and NGINX are the best-in-class web server and application delivery solutions used by high‑traffic websites such as Dropbox, Netflix, and Zynga. More than 447 million websites worldwide, including the majority of the 100,000 busiest websites, rely on NGINX Plus and NGINX to deliver their content quickly, reliably, and securely.

NGINX makes hardware load balancers obsolete. As a software-only open source load balancer, NGINX is less expensive and more configurable than hardware load balancers, and is designed for modern cloud architectures. NGINX Plus supports on-the-fly reconfiguration and integrates with modern DevOps tools for easier monitoring.
NGINX is a multifunction tool. With NGINX, you can use the same tool as your load balancer, reverse proxy, content cache, and web server, minimizing the amount of tooling and configuration your organization needs to maintain. NGINX offers tutorials, webinars, and a wide array of documentation to get you on your feet. NGINX Plus includes rapid-response customer support, so you can easily get help diagnosing any part of your stack that uses NGINX or NGINX Plus.
NGINX keeps evolving. For the past decade NGINX has been at the forefront of development of the modern Web, and has helped lead the way on everything from HTTP/2 to microservices support. As development and delivery of web applications continue to evolve, NGINX Plus keeps adding features to enable flawless application delivery, from the recently announced support for configuration using an implementation of JavaScript customized for NGINX, to support for dynamic modules. Using NGINX Plus ensures you’ll stay at the cutting edge of web performance.

# Features of nginx

1. Load Balancing 
1. Caching 
1. Reverse 

### Step to install nginx
```sh
$ sudo apt-get install nginx
```

### To start nginx
```sh
$ sudo /etc/init.d/nginx start
```

### To restart nginx
```sh
$ sudo /etc/init.d/nginx restart
```

### To stop nginx
```sh
$ sudo /etc/init.d/nginx stop
```




