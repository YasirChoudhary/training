# Fairbridge Web Api
****
## Getting Started

##### These instructions will get you a copy of the project up and running on your Server for development and testing purposes.

## Steps to Setup Project On Server

### step 1:
##### Create a droplet on Digital Ocean

### Step 2:
##### ssh to the Server
```sh
$ ssh ubuntu@xxx.xxx.xx.xx
```
### Step 3:
##### make a directory Code and Install
```sh
$ mkdir Code Install
```

### Step 4:
##### Install Dependencies
```sh
$ sudo apt-get install git python-virtualenv python-dev libssl-dev postgresql libpq-dev postgresql-client postgresql-contrib postgresql-client-common libxml2 libxml2-dev libxslt1-dev libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev build-essential libffi-dev libpulse-dev
```

### Step 5:
##### clone a repository in a Code directory
```sh
$ cd ~/Code
$ git clone git@bitbucket.org:fafadiatech/fairbridge-webapp-api.git
```

### Step 6:
##### Create and Activate virtual environment
```sh
$ cd ~/Install
$ virtualenv fairbridge_web_api
$ source fairbridge_web_api/bin/activate
```

### Step 7:
##### Install requirements
```sh
$ cd ~/Code/fairbridge-webapp-api
$ pip install -r requirements.txt 
```

### Step 8:
##### Configure PostgreSQL to start up upon server boot.
```sh
$ sudo update-rc.d postgresql enable
```

### Step 9:

##### Creating database, adding access on PostgreSQL
##### Creating database
```sh
$ sudo -u postgres createdb fairbridge
```

##### Alter password
```sh
$ sudo -u postgres psql
postgres=# alter user postgres with encrypted password 'postgres';
postgres=# grant all privileges on database fairbridge  to postgres ;
```
### Step 10:
##### Check database connection
```sh
$ psql -U postgres -h localhost -W fairbridge
```
### Step 11:
##### Configure pg_hba.conf file
```sh
$ cd /etc/postgresql/9.3/main
$ sudo nano pg_hba.conf 
```
Change ```local all all peer``` to ```local all all md5```, Save and Exit
```sh
$ sudo service postgresql restart
```

### Step 12:
##### migrate the tables
```sh
$ python manage.py migrate
```
##### Give the STATIC_ROOT path in base.py file and then migrate the static files
```sh
$ python manage.py collectstatic
```
##### Then test the app
```sh
$ python manage.py runserver 0.0.0.0:8000
```

### Step 13:

##### Install supervisor and gunicorn in environment
```sh
$ pip install supervisor gunicorn
```
##### Install nginx using apt
```sh
$ sudo apt-get install nginx
```
##### Install redis-server
```sh
$ sudo apt-get install redis-server
```

### Step 14:
##### Configure supervisor and gunicorn
```sh
$ cd ~/Install/fairbridge_web_api
$ mkdir etc
$ cd etc
$ nano supervisord.conf
[inet_http_server]
port = 127.0.0.1:9001

[supervisord]
logfile = /home/ubuntu/Install/fairbridge_web_api/etc/supervisord.log
logfile_maxbytes = 50MB
logfile_backups=10
loglevel = debug
pidfile = /home/ubuntu/Install/fairbridge_web_api/etc/supervisord.pid
nodaemon = false
minfds = 1024
minprocs = 200

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl = http://127.0.0.1:9001

[program:fairbridge]
command = /home/ubuntu/Install/fairbridge_web_api/bin/gunicorn -b 127.0.0.1:5000 --access-logfile /home/ubuntu/Install/fairbridge_web_api/etc/access.log --error-logfile /home/ubuntu/Install/fairbridge_web_api/etc/error.log -w 2 -u ubuntu --log-level debug fairbridge.wsgi
directory = /home/ubuntu/Code/fairbridge-webapp-api/fairbridge/
user = ubuntu
autostart = true
autorestart = true
startretries = 3
stopsignal = TERM
stdout_logfile = /home/ubuntu/Install/fairbridge_web_api/etc/fairbridge.stdout.log
stderr_logfile = /home/ubuntu/Install/fairbridge_web_api/etc/fairbridge.stderr.log

[program:celery]
command = /home/ubuntu/Install/fairbridge_web_api/bin/python /home/ubuntu/Code/fairbridge-webapp-api/fairbridge/manage.py celeryd -f /home/ubuntu/Install/fairbridge_web_api/etc/celery.log
directory = /home/ubuntu/Code/fairbridge-webapp-api/fairbridge/
user = ubuntu
```

### Step 15:
##### Create required log files as given in supervisor config file
```sh
$ touch /home/ubuntu/Install/fairbridge_web_api/etc/supervisord.log
$ touch /home/ubuntu/Install/fairbridge_web_api/etc/access.log
$ touch /home/ubuntu/Install/fairbridge_web_api/etc/error.log
$ touch /home/ubuntu/Install/fairbridge_web_api/etc/fairbridge.stdout.log
$ touch /home/ubuntu/Install/fairbridge_web_api/etc/fairbridge.stderr.log
$ touch /home/ubuntu/Install/fairbridge_web_api/etc/celery.log
```

### Step 16:
##### Configure nginx
```sh
$ cd /etc/nginx/conf.d/
$ sudo nano fairbridge.conf
server {

    listen   80;
    server_name 192.168.2.119;
    client_max_body_size 10M;

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    location /static {
        alias /home/ubuntu/Code/fairbridge-webapp-api/fairbridge/static;
    }

    location / {
        proxy_pass http://127.0.0.1:5000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_read_timeout 300;
     }

}
```

### Step 16:
##### Start supervisor and nginx
```sh
$ cd ~/Install/fairbridge_web_api/etc
$ supervisord -c supervisord.conf
```
##### And then start nginx
```sh
$ sudo /etc/init.d/nginx start
```

### Step 17:
##### Enter server ip in the broswer which you have configured

#### Note
```sh
$ supervisord -c supervisord.conf
$ supervisorctl status
$ supervisorctl update
$ supervisorctl restart all
$ sudo /etc/init.d/nginx start
$ sudo /etc/init.d/nginx configtest
$ sudo /etc/init.d/nginx restart
$ sudo /etc/init.d/redis-server status
$ sudo /etc/init.d/redis-server restart
$ sudo /etc/init.d/redis-server stop
```

## Built With
[django](https://docs.djangoproject.com/en/2.0/releases/1.8/)- The web framework used

## Contributing
##### When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

##### Please note we have a code of conduct, please follow it in all your interactions with the project.

## Authors

* **Sidharth Shah** - *Initial work*
* **Yogesh Panchal**
* **Yasir Choudhary**


## License

##### This project is licensed under the MIT License.

## Acknowledgments
##### Thanks to Sidharth Shah, Yogesh Panchal and Jitendra Anirudha Varma for their support










