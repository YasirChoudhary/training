# Fairbridge Pentry Web Api
****
## Getting Started

##### These instructions will get you a copy of the project up and running on your Server for development and testing purposes.

## Steps to Setup Project On Server
### Step 1:
##### Create droplet on Digital Ocean

### Step 2:
##### ssh to the server
```sh
$ ssh ubuntu@xxx.xxx.xx.xx
```
### Step 3:
##### make a directory Code and Install
```sh
$ mkdir Code Install
```
### Step 4:
##### Install Dependencies
```sh
$ sudo apt-get install git python-virtualenv python-dev libssl-dev build-essential libffi-dev libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev
```
### Step 5:
##### clone a repository in a Code directory
```sh
$ cd ~/Code
$ git clone git@bitbucket.org:fafadiatech/fairbridge_pantry_webapp_api.git
```
### Step 6:
##### Create and Activate virtual environment
```sh
$ cd ~/Install
$ virtualenv fbpantry
$ source fbpantry/bin/activate
```

### Step 7:
##### Install requirements
```sh
$ cd ~/Code/fairbridge_pantry_webapp_api
$ pip install -r requirement.txt 
```

### Step 8:
##### Install MongoDb
##### Import the public key used by the package management system.
```sh
$ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
```
##### Create a list file for MongoDB.
###### Create the /etc/apt/sources.list.d/mongodb-org-3.6.list list file using the command appropriate for your version of Ubuntu:

###### Ubuntu 14.04
```sh
$ echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
```
###### Ubuntu 16.04
```sh
$ echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
```
##### Reload local package database.

###### Issue the following command to reload the local package database:
```sh
$ sudo apt-get update
```
##### Install the MongoDB packages.
###### Install the latest stable version of MongoDB.
###### Issue the following command:
```sh
$ sudo apt-get install -y mongodb-org
```
##### Verify MongoDB is running or not.
###### Issue the following command to check status of mongod:
```sh
$ sudo service mongod status
```
### Step 9:
##### Start the mongo Shell
###### go to your <mongodb installation dir>:
```sh
$ cd /usr
```
###### Type ./bin/mongo to start mongo:
```sh
$ ./bin/mongo
```
##### Working with the mongo Shell
###### To display the database you are using, type db:
```sh
> db
```
###### To create new database type: use DBNAME
```sh
> use fairbridge_api
```
###### To create user, pwd and roles 
```sh
> db.createUser( { user: "fbUser", pwd: "fair@bridge", roles: [ "readWrite", "dbAdmin" ] })
```

###### Insert dummy data in the database
```sh
> db.fairbridge_api.insert({"yasir":true})
```
###### Then check the db is created or not
```sh
> show dbs
```

### Step 10:
##### To check wheter app is running or not
```sh
$ cd ~/Code/fairbridge_pantry_webapp_api/webapp
$ python app.py 
```

### Step 11:
##### Install supervisor and gunicorn in environment
```sh
$ pip install supervisor gunicorn
```
##### Install nginx using apt
```sh
$ sudo apt-get install nginx
```
### Step 12:
##### Configure supervisor and gunicorn
```sh
$ cd ~/Install/fbpantry
$ mkdir etc
$ nano supervisord.conf
[inet_http_server]
port = 127.0.0.1:9001

[supervisord]
logfile = /home/ubuntu/Install/fbpantry/etc/supervisord.log
logfile_maxbytes = 50MB
logfile_backups=10
loglevel = debug
pidfile = /home/ubuntu/Install/fbpantry/etc/supervisord.pid
nodaemon = false
minfds = 1024
minprocs = 200

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl = http://127.0.0.1:9001

[program:fbpantry]
command = /home/ubuntu/Install/fbpantry/bin/gunicorn -b 127.0.0.1:5000 --access-logfile /home/ubuntu/Install/fbpantry/etc/access.log --error-logfile /home/ubuntu/Install/fbpantry/etc/error.log -w 2 -u ubuntu --log-level debug app:app
directory = /home/ubuntu/Code/fairbridge_pantry_webapp_api/webapp/
user = ubuntu
autostart = true
autorestart = true
startretries = 3
stopsignal = TERM
stdout_logfile = /home/ubuntu/Install/fbpantry/etc/fbpantry.stdout.log
stderr_logfile = /home/ubuntu/Install/fbpantry/etc/fbpantry.stderr.log
```

### Step 13:
##### Create required log files as given in supervisor config file
```sh
$ touch /home/ubuntu/Install/fbpantry/etc/supervisord.log
$ touch /home/ubuntu/Install/fbpantry/etc/access.log
$ touch /home/ubuntu/Install/fbpantry/etc/fbpantry.stdout.log
$ touch /home/ubuntu/Install/fbpantry/etc/fbpantry.stderr.log
```

### Step 14:
##### Configure nginx
```sh
$ cd /etc/nginx/conf.d/
$ sudo nano fbpantry.conf
server {

    listen   80;
    server_name 192.168.2.116;
    client_max_body_size 10M;

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    location /static {
        alias /home/ubuntu/Code/fairbridge_pantry_webapp_api/webapp/static;
    }

    location / {
        proxy_pass http://127.0.0.1:5000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_read_timeout 300;
     }

}
```

### Step 15:
##### Start supervisor and nginx
```sh
$ cd ~/Install/fbpantry/etc
$ supervisord -c supervisord.conf 
```

##### And then start nginx
```sh
$ sudo /etc/init.d/nginx start
```
### Step 16:
##### Enter server ip in the broswer which you have configured

#### Note
```sh
$ supervisord -c supervisord.conf
$ supervisorctl status
$ supervisorctl update
$ supervisorctl restart all
$ sudo /etc/init.d/nginx start
$ sudo /etc/init.d/nginx configtest
$ sudo /etc/init.d/nginx restart
```

## Built With
[Flask](http://flask.pocoo.org/docs/0.12/)- The web framework used

## Contributing
##### When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

##### Please note we have a code of conduct, please follow it in all your interactions with the project.

## Authors

* **Sidharth Shah** - *Initial work*
* **Yogesh Panchal**
* **Yasir Choudhary**


## License

##### This project is licensed under the MIT License.

## Acknowledgments
##### Thanks to Sidharth Shah and Yogesh Panchal for their support










