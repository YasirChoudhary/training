# Matchapp

```sh
$ ssh ubuntu@xxx.xxx.xx.x
```
```sh
$ mkdir Code Install
$ cd Code
$ git clone https://bitbucket.org/fafadiatech/matchapp/src/master/
```

```sh
$ cd Install
$ sudo apt-get install python-virtualenv
$ virtualenv matchapp
$ source matchapp/bin/activate
$ sudo apt-get install build-essential python-dev libffi-dev zlib1g-dev libtiff5-dev libjpeg8-dev zlib1g-dev libssl-dev
```
```sh
$ pip install -r requirements.txt
```

#### Download Postgresq
To install PostgreSQL, as well as the necessary server software, run the following command:

```sh
$ apt-get install postgresql postgresql-contrib
```
Configure PostgreSQL to start up upon server boot.
```sh
$ sudo update-rc.d postgresql enable
```
Start PostgreSQL.
```sh
$ service postgresql start
```
Install dependencies 
```sh
$ sudo apt-get install postgresql libpq-dev postgresql-client postgresql-client-common
```
 (For creating user refer this link)[https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e]

### Creating user, database and adding access on PostgreSQL
As the default configuration of Postgres is, a user called postgres is made on and the user postgres has full superadmin access to entire PostgreSQL instance running on your OS.
```sh
$ sudo -u postgres psql
```
The above command gets you the psql command line interface in full admin mode.

Creating user
```sh
$ sudo -u postgres createuser <username>
```
Creating Database
```sh
$ sudo -u postgres createdb <dbname>
```
Giving the user a password
```sh
$ sudo -u postgres psql
psql=# alter user <username> with encrypted password '<password>';
```
Granting privileges on database
```sh
psql=# grant all privileges on database <dbname> to <username> ;
```
check the connection by typing
```sh
$ psql -U Username -h localhost -W DatabaseName
```

check the connection by typing
```sh
$ psql -U yasir -h localhost -W matchapp
```
### 
Install below dependency other wise it will give error while migrating database
```sh
$ pip install psycopg2-binary
```

```sh
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py collectstatic
```
To test the app
```sh
$ python manage.py runserver 0.0.0.0:8000
```
```sh
$ pip install supervisor gunicorn
$ sudo apt-get install nginx
```
Create **etc** directory in ~/Install/projectname/etc
```sh
$ mkdir ~/Install/matchapp/etc
```
Create supervisord.conf file in **etc** directory
```sh
$ cd ~/Install/matchapp/etc
$ nano supervisord.conf
[inet_http_server]
port = 127.0.0.1:9001

[supervisord]
logfile = /home/yasir/Install/matchapp/etc/supervisord.log
logfile_maxbytes = 50MB
logfile_backups=10
loglevel = debug
pidfile = /home/yasir/Install/matchapp/etc/supervisord.pid
nodaemon = false
minfds = 1024
minprocs = 200

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl = http://127.0.0.1:9001

[program:matchapp]
command = /home/yasir/Install/matchapp/bin/gunicorn -b 127.0.0.1:5050 --access-logfile /home/yasir/Install/matchapp/etc/access.log --error-logfile /home/yasir/Install/matchapp/etc/error.log -w 2 -u yasir --log-level debug xchangeplay.wsgi
directory = /home/yasir/Code/master/xchangeplay/
user = yasir
autostart=true
autorestart=true
startretries=3
stopsignal=TERM
stdout_logfile= /home/yasir/Install/matchapp/etc/matchapp.stdout.log
stderr_logfile= /home/yasir/Install/matchapp/etc/matchapp.stderr.log

[program:celery]
command = /home/yasir/Install/matchapp/bin/python /home/yasir/Code/master/xchangeplay/manage.py celeryd -f /home/yasir/Install/matchapp/etc/celery.log
directory = /home/yasir/Code/master/xchangeplay/
user = yasir
```

Then create the log files
```sh
$ touch /home/yasir/Install/matchapp/etc/supervisord.log
$ touch /home/yasir/Install/matchapp/etc/access.log
$ touch /home/yasir/Install/matchapp/etc/error.log
$ touch /home/yasir/Install/matchapp/etc/matchapp.stdout.log
$ touch /home/yasir/Install/matchapp/etc/matchapp.stderr.log
$ touch /home/yasir/Install/matchapp/etc/celery.log
```

Goto cd ~/etc/nginx/conf.d and create project.conf
```sh
$ sudo nano matchapp.conf

server {

    listen   80;
    server_name 192.168.2.114;
    client_max_body_size 10M;

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    location /static {
        alias /home/yasir/Code/master/xchangeplay/static;
    }

    location / {
        proxy_pass http://127.0.0.1:5050;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_read_timeout 300;
     }

}

```

Goto ~/Install/projectname/etc and type below command
```sh
$ supervisord -c supervisord.conf
```

And then start nginx
```sh
$ sudo /etc/init.d/nginx start
```

#### Note
```sh
$ supervisord -c supervisord.conf
$ supervisorctl status
$ supervisorctl update
$ supervisorctl restart all
$ sudo /etc/init.d/nginx start
$ sudo /etc/init.d/nginx configtest
$ sudo /etc/init.d/nginx restart
```
  



 
 




