### Automatically restart services on server reebot
1. Suppose you want to restart odoo on server reboot than:
    1. Activate the environment ` source envs/odoo/bin/activate `
    2. After that type the command `which python` It will give the fullpath of python eg.`/opt/odoo/envs/odoo/bin/python`
    3. In bin directory there is a `supervisord` binary, To restart odoo you need to require supervisord path. Copy that path.
    4. Open `rc.local` file by typing command `sudo nano /etc/rc.local`
    and than paste the `supervisord` path before `exit 0` statement.
    5. To test above configuration restart the server, after restart odoo will automatically restart.
