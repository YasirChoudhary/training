from fabric.api import run, env, sudo, cd, prefix, hide, settings, prompt

from contextlib import contextmanager as _contextmanager


database_name = "prod_add"
host_ip = "192.168.2.121"
env_name = "effactor"
postgres_uname = "estimatron"
postgres_upasswd = "estimatron"
port_no = 8050
admin_pass = "admin@1122"


all_host = "['127.0.0.1', '192.168.2.121', 'ava.efffactor.com']"


env.hosts = [host_ip]
env.user = "ubuntu"
env.password = "Ftech@1234"
env.activate = "source /home/ubuntu/Install/{}/bin/activate".format(env_name)


def make_directory():
    """
    Create a Code and Install directory
    """
    with cd('/home/ubuntu'):
        run("mkdir Code Install")


def install_dependencies():
    """
    Install all the required dependencies
    """
    sudo("apt-get update")
    sudo("apt-get install -y python-virtualenv python-dev libssl-dev postgresql libpq-dev postgresql-client \
          postgresql-contrib postgresql-client-common libxml2-dev  libxslt-dev libcairo2 libpango1.0-0  \
          libgdk-pixbuf2.0-0 libffi-dev shared-mime-info")


def clone_git_repository():
    """
    Clone git repository 
    Before running this command you need to add ssh key on Bitbucket
    """
    sudo("apt-get -y install git")
    with cd("/home/ubuntu/Code"):
        run("git clone git@bitbucket.org:fafadiatech/efffactor.git")


def create_virtualenv(env=env_name):
    """
    Create a Virtual Environment
    """
    with cd("/home/ubuntu/Install"):
        run("virtualenv {}".format(env))


@_contextmanager
def virtualenv():
    """
    Activate virtual environment
    """
    with prefix(env.activate):
        yield


def install_requirements():
    """
    Install pip requirements
    """
    with virtualenv():
        with cd("/home/ubuntu/Code/efffactor/webapp/estimatron"):
            run("pip install -r req.txt")
            run("pip freeze")


##################################################################################
#Creating user, database and adding access on PostgreSQL
def _run_as_pg(command):
    """
    Run command as 'postgres' user
    """
    with cd('/var/lib/postgresql'):
        return sudo('sudo -u postgres %s' % command)


def user_exists(name):
    """
    Check if a PostgreSQL user exists.
    """
    with settings(hide('running', 'stdout', 'stderr', 'warnings'), warn_only=True):
        res = _run_as_pg('''psql -t -A -c "SELECT COUNT(*) FROM pg_user WHERE usename = '%(name)s';"''' % locals())
    return (res == "1")



def create_user(name, password, superuser=False, createdb=False,
                createrole=False, inherit=True, login=True,
                connection_limit=None, encrypted_password=False):
    """
    Create a PostgreSQL user.

    Example::

        import fabtools

        # Create DB user if it does not exist
        if not fabtools.postgres.user_exists('dbuser'):
            fabtools.postgres.create_user('dbuser', password='somerandomstring')

        # Create DB user with custom options
        fabtools.postgres.create_user('dbuser2', password='s3cr3t',
            createdb=True, createrole=True, connection_limit=20)

    """
    options = [
        'SUPERUSER' if superuser else 'NOSUPERUSER',
        'CREATEDB' if createdb else 'NOCREATEDB',
        'CREATEROLE' if createrole else 'NOCREATEROLE',
        'INHERIT' if inherit else 'NOINHERIT',
        'LOGIN' if login else 'NOLOGIN',
    ]
    if connection_limit is not None:
        options.append('CONNECTION LIMIT %d' % connection_limit)
    password_type = 'ENCRYPTED' if encrypted_password else 'UNENCRYPTED'
    options.append("%s PASSWORD '%s'" % (password_type, password))
    options = ' '.join(options)
    _run_as_pg('''psql -c "CREATE USER %(name)s %(options)s;"''' % locals())


def database_exists(name):
    """
    Check if a PostgreSQL database exists.
    """
    with settings(hide('running', 'stdout', 'stderr', 'warnings'),
                  warn_only=True):
        return _run_as_pg('''psql -d %(name)s -c ""''' % locals()).succeeded


def create_database(name, owner, template='template0', encoding='UTF8',
                    locale='en_US.UTF-8'):
    """
    Create a PostgreSQL database.

    fab create_database:name=efffactor,owner=estimatron
    
    Example::

        import fabtools

        # Create DB if it does not exist
        if not fabtools.postgres.database_exists('myapp'):
            fabtools.postgres.create_database('myapp', owner='dbuser')

    """
    _run_as_pg('''createdb --owner %(owner)s --template %(template)s \
                  --encoding=%(encoding)s --lc-ctype=%(locale)s \
                  --lc-collate=%(locale)s %(name)s''' % locals())

##################################################################################


def add_allowed_host():
    """
    Add Host in settings.py
    """
    with cd("/home/ubuntu/Code/efffactor/webapp/estimatron/estimatron"):
        run("sed -i \"s/ALLOWED_HOSTS.*/ALLOWED_HOSTS\=\{}/g\" settings.py".format(all_host))


def export_db(db=database_name):
    """
    Export Database
    """
    sudo("export DBNAME={}".format(db))


def rm_migration():
    """
    Remove migration file and then recreate it.
    """
    with cd("/home/ubuntu/Code/efffactor/webapp/estimatron/e"):
        run("rm -rf migrations")
        run("mkdir migrations")
        with cd("/home/ubuntu/Code/efffactor/webapp/estimatron/e/migrations"):
            run("touch __init__.py")


def rm_cr_pg_hba():
    """
    Delete pg_hba.conf and then recreate with this function.
    Otherwise we will get the databse error. 
    """
    with cd("/etc/postgresql/9.3/main"):
        sudo("rm -rf pg_hba.conf")
        sudo("touch pg_hba.conf")
        sudo("echo -e 'local   all             postgres                                peer \
              \nlocal   all             all                                     md5 \
              \nhost    all             all             127.0.0.1/32            md5 \
              \nhost    all             all             ::1/128                 md5 \
            ' >> pg_hba.conf")


def restart_postgres_service():
    """
    Restart Postgres server.
    """
    sudo("service postgresql restart")


def makemigratios():
    """
    Makemigration using manage.py
    """
    with virtualenv():
        with cd("/home/ubuntu/Code/efffactor/webapp/estimatron"):
            run("./manage.py makemigrations")


def migrate():
    """
    Migrate the changes to the database.
    """
    with virtualenv():
        with cd("/home/ubuntu/Code/efffactor/webapp/estimatron"):
            CMD = "export DBNAME={} && ./manage.py migrate".format(database_name)
            run(CMD)


def create_ini_file(db=database_name):
    """
    Create ini file
    """
    with cd("/home/ubuntu/Code/efffactor/webapp/estimatron/uwsgi-configs-production"):
        run("touch {}.ini".format(db))


def write_in_ini_file(db=database_name,port=port_no):
    """
    Write the content in ini file
    """
    with cd("/home/ubuntu/Code/efffactor/webapp/estimatron/uwsgi-configs-production"):
        run("echo -e '[uwsgi]\nproject = estimatron\nbase_dir = /home/ubuntu/Code/efffactor/webapp/ \
            \nchdir = %(base_dir)/%(project)\nwsgi-file = %(base_dir)/%(project)/estimatron/wsgi.py \
            \nlogto = %(base_dir)/logs/uwsgi-{}.log\nprocesses = 1\nmaster = true \
            \nsocket = 127.0.0.1:{}\nenv = DBNAME={}' >> {}.ini".format(db,port,db,db))

def createsuperuser():
    """
    createsuperuser for the website
    """
    with virtualenv():
        with cd("/home/ubuntu/Code/efffactor/webapp/estimatron"):
            USER_CMD = "export DBNAME={} && echo \"from django.contrib.auth.models import User; User.objects.create_superuser(\'admin\', \'admin@gmail.com\', \'{}\')\" | ./manage.py shell".format(database_name,admin_pass)
            run(USER_CMD)
            GROUP_CMD = "export DBNAME={} && echo \"from django.contrib.auth.models import Group; Group.objects.create(name=\'ADMINS\')\" | ./manage.py shell".format(database_name)
            run(GROUP_CMD)
            GROUP_ADD = "export DBNAME={} && echo \"from django.contrib.auth.models import User; from django.contrib.auth.models import Group; u=User.objects.get(username=\'admin\'); u.groups.add(Group.objects.get(name=\'ADMINS\')); u.save()\" | ./manage.py shell".format(database_name)
            run(GROUP_ADD)


def create_log_file():
    """
    Make directory logs and the create emperor log file
    """
    with cd("/home/ubuntu/Code/efffactor/webapp"):
        run("mkdir logs")
        with cd("/home/ubuntu/Code/efffactor/webapp/logs"):
            run("touch uwsgi-emperor-production.log")


def uwsgi_efffactor_log(db=database_name):
    """
    Create Individual log file for individual ini file.
    """
    with cd("/home/ubuntu/Code/efffactor/webapp/logs"):
        run("touch uwsgi-{}.log".format(db))


def rm_cr_start_uwsgi_prod():
    """
    Remove old start_uwsgi_production.sh and recreate using the below content by running this function.
    """
    with cd("/home/ubuntu/Code/efffactor/webapp/estimatron"):
        run("rm -rf start_uwsgi_production.sh")
        run("touch start_uwsgi_production.sh")
        run("echo -e '#!/bin/sh\n/home/ubuntu/Install/{}/bin/uwsgi --master --emperor uwsgi-configs-production --die-on-term --logto ../logs/uwsgi-emperor-production.log' >> start_uwsgi_production.sh".format(env_name))
        sudo("chmod +x start_uwsgi_production.sh")


def start_uwsgi_file():
    """
    Start start_uwsgi_production.sh using this function.
    """
    with cd("/home/ubuntu/Code/efffactor/webapp/estimatron"):
        run("(nohup ./start_uwsgi_production.sh &> /dev/null < /dev/null &) && /bin/true")


def install_nginx():
    """
    Install nginx
    """
    sudo("apt-get -y install nginx")


def configure_nginx(db=database_name, ip=host_ip, port=port_no):
    """
    Configure nginx file.
    """
    with cd("/etc/nginx/conf.d"):
        sudo("touch {}.conf".format(db))
        sudo("echo -e 'server {{ \
            \nlisten       80; \
            \nserver_name  {}; \
            \n\
            \naccess_log  /var/log/nginx/{}_nginx_access.log; \
            \nerror_log   /var/log/nginx/{}.log; \
            \n\
            \n    location /static/ {{ \
            \n        alias /home/ubuntu/Code/efffactor/webapp/estimatron/static/; \
            \n        access_log off; \
            \n    }} \
            \n\
            \n    location / {{ \
            \n        include uwsgi_params; \
            \n        uwsgi_pass 127.0.0.1:{}; \
            \n        uwsgi_read_timeout 300; \
            \n    }} \
            \n}}' >> {}.conf".format(ip,db,db,port,db))


def restart_nginx():
    """
    Restart nginx
    """
    sudo("/etc/init.d/nginx restart")


def deploy():
    """
    When you want to deploy new instance on new virtual-environment on new server run this function.
    But make sure you need to change database_name and host_ip or server_ip at the start of this script 
    as per requirement.
    """
    make_directory()
    install_dependencies()
    clone_git_repository()
    create_virtualenv(env=env_name)
    virtualenv()
    install_requirements() #Done
    create_user(name=postgres_uname, password=postgres_upasswd) #fab create_user:name=estimatron,password=estimatron
    create_database(name=database_name, owner=postgres_uname) #fab create_database:name=efffactor,owner=estimatron
    add_allowed_host()
    export_db(db=database_name)
    rm_migration()
    rm_cr_pg_hba()
    restart_postgres_service()
    makemigratios()
    migrate()           
    create_ini_file(db=database_name)
    write_in_ini_file(db=database_name,port=port_no)
    createsuperuser() 
    create_log_file()
    uwsgi_efffactor_log(db=database_name)
    rm_cr_start_uwsgi_prod()
    start_uwsgi_file() 
    install_nginx()
    configure_nginx(db=database_name, ip=host_ip, port=port_no)
    restart_nginx()


def deploy_new_instance():
    """
    When you want to deploy an efffactor on existing virtual-environment run this function.
    But make sure you need to change database_name and host_ip or server_ip at the start of this script 
    as per requirement. 
    """
    virtualenv()
    create_database(name=database_name, owner=postgres_uname)
    add_allowed_host()
    export_db(db=database_name)
    migrate()           
    create_ini_file(db=database_name)
    write_in_ini_file(db=database_name,port=port_no)
    createsuperuser()
    uwsgi_efffactor_log(db=database_name)
    configure_nginx(db=database_name, ip=host_ip, port=port_no)
    restart_nginx()