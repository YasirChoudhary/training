# Effactor
### Step 1: 
```sh
$ sudo apt-get install openssh-server
```
### Step 2:
```sh
$ ssh ubuntu@192.168.2.126
$ mkdir Code Instal 
$ sudo apt-get install git
```
### Step 3:
```sh
$ cd Code
$ git clone https://YasirChoudhary@bitbucket.org/fafadiatech/efffactor.git
```

### Step 4:
```sh
$ sudo apt-get install python-virtualenv python-dev libssl-dev postgresql libpq-dev postgresql-client postgresql-contrib postgresql-client-common libxml2-dev  libxslt-dev libcairo2 libpango1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info
```

### Step 5:
Configure PostgreSQL to start up upon server boot.
```sh
$ sudo update-rc.d postgresql enable
```
### Step 6:
Create and activate Virtualenvironment 
```sh
$ cd Install
$ virtualenv effactor
$ source effactor/bin/activate
```
### Step 7:
```sh
$ cd ~/Code/efffactor/webapp/estimatron 
$ ls
$ pip install -r req.txt 
```
### Step 8:
Creating user, database and adding access on PostgreSQL
##### Creating user

```sh
$ sudo -u postgres createuser estimatron
```
##### Creating Database
```sh
$ sudo -u postgres createdb efffactor
```
##### Giving the user a password
```sh 
$ sudo -u postgres psql
postgres# alter user estimatron with encrypted password 'estimatron';
```
##### Granting privileges on database
```sh
postgres># grant all privileges on database efffactor to estimatron ;
postgres>#\q
```

### Step 9:
```sh
$ export DBNAME=efffactor
```
### Step 10:
```sh
$ cd ~/Code/efffactor/webapp/estimatron/e
$ rm -rf migrations
$ mkdir migrations
$ cd migrations
$ touch __init__.py
```
### Step 11:
```sh
$ cd /etc/postgresql/9.3/main
$ sudo nano pg_hba.conf 
```
Change ```local all all peer``` to ```local all all md5```, Save and Exit
```sh
$ sudo service postgresql restart
```
### Step 12:
```sh
$ cd ~/Code/efffactor/webapp/estimatron
$ ./manage.py makemigrations
$ ./manage.py migrate
```
### Step 13:
```sh
$ cd ~/Code/efffactor/webapp/estimatron/uwsgi-configs-production
$ nano efffactor.ini

[uwsgi]
project = estimatron
base_dir = /home/ubuntu/Code/efffactor/webapp/
chdir = %(base_dir)/%(project)
wsgi-file = %(base_dir)/%(project)/estimatron/wsgi.py
logto = %(base_dir)/logs/uwsgi-efffactor.log
processes = 1
master = true
socket = 127.0.0.1:8005
env = DBNAME=efffactor
```

### Step 14:
create admin user using following commands
```sh
$ cd ~/Code/efffactor/webapp/estimatron
$ export DBNAME=efffactor
$ ./manage.py createsuperuser
Username: admin
email: admin@gmail.com
password: Ftech@1234
```

### Step 15:
Start uwsgi server

```sh
$ cd ~/Code/efffactor/webapp/estimatron
$ nano start_uwsgi_production.sh
```
change the file env variable
And the create directory

```sh
$ cd ~/Code/efffactor/webapp
$ mkdir logs
$ touch uwsgi-emperor-production.log
$ cd ~/Code/efffactor/webapp/estimatron
$ ./start_uwsgi_production.sh &
```
### Step 16:
Install and Configure nginx
```sh
$ sudo apt-get install nginx
$ sudo touch /var/log/nginx/effactor_nginx_access.log
$ sudo touch /var/log/nginx/effactor.log

$ cd /etc/nginx/conf.d
$ sudo nano efffactor.conf

server {
    listen       80;
    server_name  192.168.2.126;

    access_log  /var/log/nginx/effactor_nginx_access.log;
    error_log   /var/log/nginx/effactor.log;

    location /static/ {
        alias /home/ubuntu/Code/efffactor/webapp/estimatron/static/;
        access_log off;
    }

    location / {
        include uwsgi_params;
        uwsgi_pass 127.0.0.1:8005;
        uwsgi_read_timeout 300;
    }
}
```

### Step 17:
Restart nginx server
```sh
$ sudo /etc/init.d/nginx restart
```

### Step 18:
Open Browser and type IP Which you have set in nginx configuration.

## NOTE:
If it give error 502 Bad Gateway
then check /var/log/nginx/effactor.log file
```sh
$ tail -f /var/log/nginx/effactor.log
```
 *1 upstream prematurely closed connection while reading response header from upstream

Then check the logs
```sh
$ cd ~/Code/efffactor/webapp/logs
$ tail -f uwsgi-efffactor.log
```
If it give below error
AttributeError: 'Settings' object has no attribute 'ENABLE_AUTOMATIC_BILLING'

Goto Settings.py file add the below line at the end
```sh
ENABLE_AUTOMATIC_BILLING = True
```
















