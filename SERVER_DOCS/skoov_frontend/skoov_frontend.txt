
Install apt dependencies
$ sudo apt-get install libblas-dev liblapack-dev libatlas-base-dev gfortran


Step 1:
Create and activate virtualenv
$ cd ~/Install
$ virtualenv skoov-frontend
$ source skoov-frontend/bin/activate

Step 2:
Install dependencies
$ pip install cython
$ pip install --upgrade setuptools
$ pip install SciPy==0.9
Install requirements
$ pip install -r requirements.txt


then goto skoov-fetcher parser and 
$ python setup.py install

then goto skoov-frontend
$ goto fetcher directory 
$ python qp.py install

