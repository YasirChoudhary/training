# skoov_b2c
****
## Getting Started

##### These instructions will get you a copy of the project up and running on your Server for development and testing purposes.

## Steps to Setup Project On Server

### Step 1:
##### Create a Droplet on Digital Ocean

### Step 2: 
##### ssh to server
```sh
$ ssh ubuntu@xxx.xxx.xx.xx
```
### Step 3:
##### make the directory Code and Install
```sh
$ mkdir Code Install
```
### Step 4:
##### Install dependencies
```sh
$ sudo apt-get install git python-virtualenv python-dev libssl-dev build-essential libffi-dev libtiff5-dev libpq-dev libxml2 libxml2-dev libxslt1-dev zlib1g-dev
```

### Step 5:
##### clone a repository in a Code directory
```sh
$ cd ~/Code
$ git clone git@bitbucket.org:fafadiatech/skoov-b2c.git
```
### Step 6:
##### Create and Activate virtual environment
```sh
$ cd ~/Install
$ virtualenv skoov_b2c
$ source skoov_b2c/bin/activate
```

### Step 7:
##### Install requirements
```sh
$ cd ~/Code/skoov-b2c
$ pip install -r requirements.txt 
```

### Step 8:
##### Setup Postgres database
###### Install dependencies
```sh
$ sudo apt-get install postgresql postgresql-client postgresql-contrib 
postgresql-client-common
```
###### Configure PostgreSQL to start up upon server boot.
```sh
$ sudo update-rc.d postgresql enable
```

##### Creating database, adding access on PostgreSQL
###### Creating database
```sh
$ sudo -u postgres createdb skoov
```
###### Alter password
```sh
$ sudo -u postgres psql
postgres=# alter user postgres with encrypted password 'ftech1234';
postgres=# grant all privileges on database skoov to postgres ;
postgres=# \q
```

##### Check database connection
```sh
$ psql -U postgres -h localhost -W skoov
```

### Step 9:
##### Configure pg_hba.conf file
```sh
$ cd /etc/postgresql/9.3/main
$ sudo nano pg_hba.conf
```
##### Change ```local all postgres peer``` to ```local all postgres md5```
##### Change ```local all all peer``` to ```local all all md5```, Save and Exit

```sh
$ sudo service postgresql restart
```

### Step 10:

##### Delete migrartions folder from each directory at this location ~/Code/skoov-b2c/skoov_b2c
##### Recreate migration file and inside that create __init__.py file using touch command

### Step 11:
##### migrate auth table
```sh
$ cd ~/Code/skoov-b2c/skoov_b2c
$ python manage.py migrate auth
```
##### Create migrations
```sh
$ python manage.py makemigrations
```
##### migrate the tables
```sh
$ python manage.py migrate
```
##### Give the STATIC_ROOT path in base.py file and then migrate the static files
```sh
$ python manage.py collectstatic
```
##### Then test the app
```sh
$ python manage.py runserver 0.0.0.0:8000
```
##### Create superuser
```sh
$ python manage.py createsuperuser
Email address: admin@gmail.com                                    
Password: Ftech@1234
```

### Step 12:
##### Install supervisor and gunicorn in environment
```sh
$ pip install supervisor gunicorn
```
##### Install nginx using apt
```sh
$ sudo apt-get install nginx 
```
##### Install redis-server
```sh
$ sudo apt-get install redis-server
```
### Step 13:
##### Configure supervisor and gunicorn
```sh
$ cd ~/Install/skoov_b2c
$ mkdir etc
$ cd etc
$ nano supervisord.conf
[inet_http_server]
port = 127.0.0.1:9001

[supervisord]
logfile = /home/ubuntu/Install/skoov_b2c/etc/supervisord.log
logfile_maxbytes = 50MB
logfile_backups=10
loglevel = debug
pidfile = /home/ubuntu/Install/skoov_b2c/etc/supervisord.pid
nodaemon = false
minfds = 1024
minprocs = 200

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl = http://127.0.0.1:9001

[program:skoov_b2c]
command = /home/ubuntu/Install/skoov_b2c/bin/gunicorn -b 127.0.0.1:5050 --access-logfile /home/ubuntu/Install/skoov_b2c/etc/access.log --error-logfile /home/ubuntu/Install/skoov_b2c/etc/error.log -w 2 -u ubuntu --log-level debug skoov_b2c.wsgi
directory = /home/ubuntu/Code/skoov-b2c/skoov_b2c/
user = ubuntu
autostart = true
autorestart = true
startretries = 3
stopsignal = TERM
stdout_logfile = /home/ubuntu/Install/skoov_b2c/etc/skoov_b2c.stdout.log
stderr_logfile = /home/ubuntu/Install/skoov_b2c/etc/skoov_b2c.stderr.log

[program:celery]
command = /home/ubuntu/Install/skoov_b2c/bin/python /home/ubuntu/Code/skoov-b2c/skoov_b2c/manage.py celeryd -f /home/ubuntu/Install/skoov_b2c/etc/celery.log
directory = /home/ubuntu/Code/skoov-b2c/skoov_b2c/
user = ubuntu
```

### Step 14:
##### Create required log files as given in supervisor config file
```sh
$ touch /home/ubuntu/Install/skoov_b2c/etc/supervisord.log
$ touch /home/ubuntu/Install/skoov_b2c/etc/access.log
$ touch /home/ubuntu/Install/skoov_b2c/etc/error.log
$ touch /home/ubuntu/Install/skoov_b2c/etc/skoov_b2c.stdout.log
$ touch /home/ubuntu/Install/skoov_b2c/etc/skoov_b2c.stderr.log
$ touch /home/ubuntu/Install/skoov_b2c/etc/celery.log
```

### Step 15:
##### Configure nginx
```sh
$ cd /etc/nginx/conf.d/
$ sudo nano skoov_b2c.conf
server {

    listen   80;
    server_name 192.168.2.114;
    client_max_body_size 10M;

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    location /static {
        alias /home/ubuntu/Code/skoov-b2c/skoov_b2c/static;
    }

    location / {
        proxy_pass http://127.0.0.1:5050;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_read_timeout 300;
     }

}
```

### Step 16:
##### Start supervisor and nginx
```sh
$ cd ~/Install/skoov_b2c/etc
$ supervisord -c supervisord.conf
```

##### And then start nginx
```sh
$ sudo /etc/init.d/nginx start
```

### Step 17:

##### Enter server ip in the broswer which you have configured

#### Note
```sh
$ supervisord -c supervisord.conf
$ supervisorctl status
$ supervisorctl update
$ supervisorctl restart all
$ sudo /etc/init.d/nginx start
$ sudo /etc/init.d/nginx configtest
$ sudo /etc/init.d/nginx restart
$ sudo /etc/init.d/redis-server status
```

## Built With
[Flask](http://flask.pocoo.org/docs/0.12/)- The web framework used

## Contributing
##### When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

##### Please note we have a code of conduct, please follow it in all your interactions with the project.

## Authors

* **Sidharth Shah** - *Initial work*
* **Yogesh Panchal**
* **Yasir Choudhary**


## License

##### This project is licensed under the MIT License.

## Acknowledgments
##### Thanks to Sidharth Shah and Yogesh Panchal for their support










