
Step 1:
$ sudo -u postgres psql

Step 2:
postgres-# CREATE USER nagios;
postgres-# grant all privileges on database fafadia to nagios;
postgres=# alter user nagios with encrypted password 'ftech1234';
postgres=# \q


Step 3:
Check connection

$ psql -U nagios -h localhost -W fafadia
