# Loanxpress
****
## Getting Started

##### These instructions will get you a copy of the project up and running on your Server for development and testing purposes.

## Steps to Setup Project On Server

### step 1:
##### Create a droplet on Digital Ocean

### Step 2: 
##### ssh to server
```sh
$ ssh ubuntu@xxx.xxx.xx.xx
```

### Step 3:
##### make a directory Code and Install
```sh
$ mkdir Code Install
```

### Step 4:
##### Install dependencies 
```sh
$ sudo apt-get install git python-dev python-virtualenv libssl-dev mysql-server libmysqlclient-dev libffi-dev libtiff5-dev libjpeg8-dev zlib1g-dev libevent-dev
```

##### Clone Repository
```sh
$ git clone git@bitbucket.org:fafadiatech/loanxpress.git
```


### Step 5:
##### Create and Activate virtual environment
```sh
$ cd ~/Install
$ virtualenv loanxpress
$ source loanxpress/bin/activate
```

### Step 6:
##### Install requirements
```sh
$ cd ~/Code/loanxpress
$ pip install -r requirements.txt 
```

### Step 7:
##### Install supervisor and gunicorn in environment
```sh
$ pip install supervisor gunicorn
```
##### Install nginx
```sh
$ sudo apt-get install nginx
```
##### Install redis-server
```sh
$ sudo apt-get install redis-server
```
### Step 8:
##### Configure supervisor and gunicorn
```sh
$ cd ~/Install/loanxpress
$ mkdir etc
$ cd etc
$ nano supervisord.conf

[inet_http_server]
port = 127.0.0.1:9001

[supervisord]
logfile = /home/ubuntu/Install/loanxpress/etc/supervisord.log
logfile_maxbytes = 50MB
logfile_backups=10
loglevel = debug
pidfile = /home/ubuntu/Install/loanxpress/etc/supervisord.pid
nodaemon = false
minfds = 1024
minprocs = 200

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

[supervisorctl]
serverurl = http://127.0.0.1:9001

[program:matchapp]
command = /home/ubuntu/Install/loanxpress/bin/gunicorn -b 127.0.0.1:5050 --access-logfile /home/ubuntu/Install/loanxpress/etc/access.log --error-logfile /home/ubuntu/Install/loanxpress/etc/error.log -w 2 -u ubuntu --log-level debug loanxpress.wsgi
directory = /home/ubuntu/Code/loanxpress/loanxpress/
user = ubuntu
autostart = true
autorestart = true
startretries = 3
stopsignal = TERM
stdout_logfile = /home/ubuntu/Install/loanxpress/etc/loanxpress.stdout.log
stderr_logfile = /home/ubuntu/Install/loanxpress/etc/loanxpress.stderr.log

[program:celery]
command = /home/ubuntu/Install/loanxpress/bin/python /home/ubuntu/Code/loanxpress/loanxpress/manage.py celeryd -f /home/ubuntu/Install/loanxpress/etc/celery.log
directory = /home/ubuntu/Code/loanxpress/loanxpress/
user = ubuntu

```

### Step 9:
##### Create required log files as given in supervisor config file
```sh
$ touch /home/ubuntu/Install/loanxpress/etc/supervisord.log
$ touch /home/ubuntu/Install/loanxpress/etc/access.log
$ touch /home/ubuntu/Install/loanxpress/etc/error.log
$ touch /home/ubuntu/Install/loanxpress/etc/loanxpress.stdout.log
$ touch /home/ubuntu/Install/loanxpress/etc/loanxpress.stderr.log
$ touch /home/ubuntu/Install/loanxpress/etc/celery.log
```

### Step 10:
##### Create database
```sh
$ mysql -u root -p
$ create db loandb;
```
##### Note: At the time installation of mysql server you should give password ```ftech#123```
If you have given different name you can also alter that password
Run below command as a ubuntu user not mysql user
```sh
$ mysqladmin -u root -p'OLDPASSWORD' password 'ftech#123'
```

### Step 11:
##### Delete migrartions folder from each directory at this location ~/Code/loanxpress/loanxpress
##### Recreate migration file and inside that create __init__.py file using touch command

### Step 12:
##### Create migrations and then migrate the tables using below command
```sh
$ cd ~/Code/loanxpress/loanxpress
$ python manage.py makemigrations
$ python manage.py migrate
```

##### Change The ```STATIC_ROOT``` path in base.py under settings directory and give directory name you want ex:dump
```sh
$ python manage.py collectstatic
Copy admin folder from dump directory and paste in ~/Code/loanxpress/loanxpress/static folder
$ python manage.py createsuperuser
Email address: admin@gmail.com
Mobile: 9999999999
Password: Ftech@1234
```

### Step 13:
##### Configure nginx 
```sh
$ cd /etc/nginx/conf.d
$ sudo nano loanxpress.conf

server {

    listen   80;
    server_name 192.168.2.122;
    client_max_body_size 10M;

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    location /static {
        alias /home/ubuntu/Code/loanxpress/loanxpress/static;
    }

    location / {
        proxy_pass http://127.0.0.1:5050;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_read_timeout 300;
     }

}
```

### Step 14:
##### Start supervisor and nginx
```sh
$ cd ~/Install/loanxpress/etc
$ supervisord -c supervisord.conf
```

##### And then start nginx
```sh
$ sudo /etc/init.d/nginx start
```

#### Note
```sh
$ supervisord -c supervisord.conf
$ supervisorctl status
$ supervisorctl update
$ supervisorctl restart all
$ sudo /etc/init.d/nginx start
$ sudo /etc/init.d/nginx configtest
$ sudo /etc/init.d/nginx restart
$ sudo /etc/init.d/redis-server status
$ sudo /etc/init.d/redis-server restart
$ sudo /etc/init.d/redis-server stop
```

## Built With
[django](https://docs.djangoproject.com/en/2.0/releases/1.8/)- The web framework used

## Contributing
##### When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

##### Please note we have a code of conduct, please follow it in all your interactions with the project.

## Authors

* **Sidharth Shah** - *Initial work*
* **Yogesh Panchal**
* **Yogesh Panchal**
* **Jitendra Anirudha Varma**
* **Hardik Kacha**
* **Yasir Choudhary**
* **Prasen Revankar**
* **Yamini**
* **Shalini**
* **Tejashree**

## License

##### This project is licensed under the MIT License.

## Acknowledgments
##### Thanks to Sidharth Shah, Yogesh Panchal and Jitendra Anirudha Varma for their support










