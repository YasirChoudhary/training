Worklog
=======

# May 2018

## 18 july 2018 - 6.00 Hours
1. Watched tutorials on accounting
1. Understood how to add manual entry in accounting and journal entries

## 05 july 2018 - 6.45 Hours
1. Tried to understand odoo workflow and added debit credit module in that.

## 04 july 2018 - 6.00 Hours
1. Tried to understand odoo workflow

## 29 june 2018 - 6.00 Hours
1. Tried to understand hr functionality of odoo and tried to understand about models.

## 27 june 2018 - 6.00 Hours
1. Setup odoo on virtualBox

## 26 june 2018 - 6.30 Hours
1. Tried to setup odoo on virtual box.

## 25 june 2018 - 6.00 Hours
1. Written a automated script to deploy efffactor on server.

## 23 june 2018 - 6.30 Hours
1. Tried to deploy effactor using fabric script

## 22 june 2018 

## 21 june 2018 - 6.30 Hours
1. Written automated script using fabric to deploy Efffactor
1. Deployed FineAI app on server


## 20 june 2018 - 6.00 HOURS
1. Tried to deploy effactor using fabric

## 19 june 2018 - 6.30 Hours
1. I ran the basic commands using fabric
1. Tried to create virtualenv and actiavted using fabric
1. Tried to create postgres user, database using fabric

## 12 june 2018 - 7.00 Hours
1. Setup nagios
1. Monitered Users, ssh, Mysql, Postgres, Harddisk, Swap Memory using Nagios

## 11 june 2018 - 7.00 Hours
1. Tried to setup nagios using check_nrpe

## 09 june 2018 - 7.00 Hours
1. Read nagios blog and watched tutorials on nagios
1. Tried to ping client machine using nagios and checked disk space using nagios.

## 08 june 2018 - 7.00 Hours
1. Read nagios blog
1. Tried to understand files and directories containing nagios

## 07 june 2018 - 7.00 Hours
1. Read nagios blog
1. Setup nagios on virtual Box

## 06 june 2018 - 7.00 Hours
1. Tried to deploy skoov-b2c

## 05 june 2018 - 7.00 Hours
1. Completed deployment of fairbridge-Pentry-Web-Api
1. Tried to deploy skoov-b2c

## 04 june 2018 - 7.00 Hours
1. I completed deployment of fairbridge-Web-Api

## 01 june 2018 - 5.30 Hours
1. Tried to deploy croma diwali app


## 31 may 2018 - 7.00 Hours
1. I Completed deployment of Loanxpress web app.

## 30 may 2018 - 7.00 Hours
1. I completed deployment of Effactor on Virtual Box

## 29 may 2018 - 7.00 Hours
1. I completed deployment of matchapp on Virtual Box

## 28 may 2018 - 7.00 Hours
1. Tried to setup solr in distributed mode.
1. Read Kaggle documentation of Machine Learning and tried to implement some codes.
 
## 26 may 2018 - 6.30 Hours
1. Tried to setup solr in distributed mode.

## 25 may 2018 - 7.00 Hours
1. Tried to setup solr in distributed mode

## 24 may 2018 - 7.00 Hours
1. Tried to setup solr in distributed mode.

## 23 may 2018 - 7.15 Hours
1. Read solr documentation and setup solr in solr cloud mode. 

## 22 may 2018 - 8.00 Hours
1. Portia: Scrapped a website on portia and created markdown file. 
1. Read about solr

## 21 may 2018 - 7.00 Hours
1. Portia: I created a project and scrapped a website on portia. mayankmodi.com, tea.com
1. self improvement : I searched on solr. 

## 18 May 2018 - 6.00 Hours
1. Tried to implement class based views

## 17 May 2018 - 6.30 Hours
1. read django documentation of class based views and watched some tutorials on that and implemented some class based views

## 16 May 2018 - 7.15 Hours
1. read django documentation of class based views and tried to implement some class based views.


## 15 May 2018 - 7.15 Hours
1. read django documentation of class based views and tried to implement some class based views.

## 14 May 2018 - 7.00 Hours
1. read django documentation of class based views and tried to implement some class based views.

## 12 may 2018 - 7.30 Hours
1. Read django documentation of class based views and tried to implement models and views
1. Tried to parse product details of myntra 

## 11 may 2018 - 7.30 Hours
1.  Read django documentation of class based views and tried to implement some views using classes.

## 10 may 2018 - 8.00 Hours
1. I tried to implement edit_task and delete_task for todo app 

## 09 may 2018 - 8:00 Hours
1. I tried to implement login and signup page for todo app.
1. I tried to connect login and sign up page with database
1. I tried to show all the users todo list by fetching database 

## 08 may 2018 - 8:00 Hours
1. Read django documentation of session
1. I tried to implement todo app and worked on views models and url module.
1. I tried to implement user and sign up page for todo app


## 07 may 2018 - 8:00 Hours
1. Read django documentation of Forms
1. I tried to implement todo app and worked on views models and url module.

## 04 may 2018 - 7:00 Hours
1. Read django documentation and practice django views, urls and template.

## 03 may 2018 - 8:00 Hours
1. Read django documentation and practice django views, urls and template.
1. Watched django tutorials
1. I Understand entity concept of Haptik Chat bot builder

## 02 may 2018 - 7:00 Hours
1. Read django documentation and practice django views, urls and template
1. Typing Practice


# APR 2018

## 30 apr 2018 - 7:00 Hours
1. Read django documentation and practice django views, urls and template
1. I done testing of Haptik Reports Chat bot

## 28 apr 2018 - 6:00 Hours
Read django documentation and practice django views, models and urls

## 27 apr 2018 - 6:00 Hours
Read django blog and practice django views, models and urls


## 26 apr 2018 - 5:00 Hours
Read blog of django and tried to implement some app


## 25 apr 2018 - 6:00 Hours
1. read some pages of mysql book
1. Understand some functionality of haptik chat bot creation
1. Read about a Class diagram from wikipedia and about CRC from blog.


## 24 apr 2018 - 5:45 Hours
1. Typing Practice
1. Read Haptik Entities Documentation
1. Tried to create a bot 

## 23 apr 2018 - 7:30 Hours
1. Read Haptik Documentation
1. Typing Practice
1. Created Node on Haptik Mogambo Chat Builder


