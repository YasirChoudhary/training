# To create user in mysql
```sh
$ CREATE USER 'yasir'@'localhost' IDENTIFIED BY 'password';
```

# To login into mysql
```sh
$ mysql -u yasir -p
```

Or

```sh
$ sudo mysql -u root
```
