# Python3
### To create virtual environment for Python3
```sh
$ virtualenv -p python3 envName
```

### To install requirements in virtual Environment
```sh
$ pip3 install -r req.txt
```

# Python2
```sh
$ virtualenv envName
```

### To install requirements in virtual Environment
```sh
$ pip install -r req.txt
```
