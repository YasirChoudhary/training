# Postgres 
### To Install PostgresSQL
```sh
$ sudo apt-get install postgresql postgresql-contrib
```

### To access database
```sh
$ sudo -u postgres psql
```

### To see the database
```sh
$ \l
```

### To change the database
```sh
$ \c DatabaseName;
```

### To see the tables in database
```sh
$ \dt
```

### To describe the table fields
```sh
$ \d TableName;
```

### To see all the data from table
```sh
$ select * from TableName;
```

### To take backup of postgres database
```sh
$ sudo su - postgres
$ pg_dump DatabaseName > DatabaseName.sql
```
Give Password of postgres if above command ask for password
##### To find the dump database
```sh
$ ls /var/lib/postgresql/
```
##### To Copy postgres in home directory
Write below command from home directory
```sh
$ cp /var/lib/postgresql/DatabaseName.sql .
```
### To Restore database
1. First create the database in postgres
```sh
$ sudo -u postgres psql
$ create database NewDatabaseName;
$ \q
```
2. To restore the database
```sh
$ sudo su - postgres
$ psql NewDatabaseName < DatabaseName.sql
```
OR
```sh
$ sudo su - postgres
$ psql -U postgres NewDatabaseName < DatabaseName.sql
```





