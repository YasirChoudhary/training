# Portia

### Step 1
1. Goto [scrapinghub.com](https://scrapinghub.com/)
2. click on Sign Up and register
3. Then logged in to the website

### Step 2 
1. Click on Create Project
2. After creation of Project take a sample url (mayankmodi.com)
3. Click on open portia and paste the url
4. When website is loaded click on New Spider
5. And after that, If you want to scrap a shirt from mayankmodi.com 
6. Click on Tops -> shirts
7. The you will see multiple shirt
8. Afer that Click on one shirt you will be redirected to new url

### Step 3
##### Click on NEW SAMPLE
1. Hover and click the title of the shirt and then give the field name.
1. Hover and click the price of shirt and then give the field name price.
1. Hover and click the image of shirt and the give field name.

##### After that click on CLOSE SAMPLE

1. Then goto top left corner and then you will see all the projects
2. After that click on the right corner of the project and then click on publish project

3. After that click on Scrappinghub dashboard
4. And then Click on Run and select the url and then again click on Run
5. After Completion of Running jobs you can click on items and then you will see all the items with fields and then you can exports those items in the format of json csv etc.
