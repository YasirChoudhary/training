import json
from json import JSONEncoder

root_cat = {"000073" : "Drone Charger" ,"000072" : "Drone Camera" ,"000071" : "Drone Battery" ,"000070" : "Drone Accessories" ,"000069" : "Backpack" ,"000065" : "Jumping Drone" ,"000068" : "GAMING CAPTURE CARD" ,"000067" : "MOUSE BUNJEE" ,"000066" : "VR Case" ,"000064" : "HDMI Cable" ,"000063" : "Gaming Speaker" ,"000061" : "Gaming Mic" ,"000059" : "Gaming Controller Shifter" ,"000058" : "Gaming Controller Wheel" ,"000057" : "Gaming Chair" ,"000056" : "Power Supply" ,"000055" : "Computer Casing" ,"000054" : "Mousepad" ,"000053" : "Laptop Accessories" ,"000052" : "Gaming Headphone" ,"000051" : "Gaming Controller" ,"000050" : "Keyboard Rest" ,"000048" : "Flash Drive" ,"000047" : "Micro Sd Card" ,"000046" : "Mobile/Tab Charger" ,"000045" : "Microphone" ,"000044" : "Case" ,"000043" : "Carry Bag" ,"000042" : "Laptop Sleeve" ,"000041" : "Selfie Stick" ,"000040" : "Carry Case" ,"000039" : "Stylus Pen" ,"000038" : "Wireless Charger" ,"000037" : "Tablet Case" ,"000036" : "Keyboard Protector" ,"000035" : "Ipad Case" ,"000033" : "Iphone Case" ,"000032" : "Headphone Stand" ,"000031" : "Stand" ,"000030" : "Sleeve" ,"000029" : "Headphone" ,"000028" : "Dj Headphone" ,"000027" : "Dj Control" ,"000026" : "Webcam" ,"000025" : "Speaker" ,"000024" : "Bluetooth Speaker" ,"000019" : "Flower Pot" ,"000016" : "Flying Drone" ,"000015" : "Mobile Phone" ,"000014" : "Monitor" ,"000013" : "Gaming Keyboard" ,"000012" : "Gaming Headset" ,"000010" : "Gaming Mouse" ,"00008" : "Charger" ,"00007" : "Tempered Glass" ,"00006" : "Screen Protector" ,"00005" : "Power Bank" ,"00004" : "Mobile Charger" ,"00003" : "Car Charger" ,"00002" : "Cable" ,"00001" : "Wall Charger"}

file_to_save = open("related_nodes.json","w")

file_read = open("Recommendations.csv","r").readlines()



dic = {}

li = []
for key,val in root_cat.items():
    li.append(key)

lis = 0

for row in file_read:
    key = li[lis]
    dic[key] = []
    for k,v in root_cat.items():
        roww = row.strip(" ")
        if v in roww:
            dic[key].append(k)
        else:
            continue
    lis +=1 

for ki,vl in dic.items():
    print(ki,vl)


json = json.dumps(dic)
f = open("related_nodes.json","w")
f.write(json)
f.close()


print("Program Executed")
