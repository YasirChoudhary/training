from boto3 import session
from botocore.client import Config
from collections import OrderedDict
import datetime
import sys
import requests
from requests.auth import HTTPBasicAuth

ACCESS_ID = 'LBFDKVGDEKTALQR2JBUJ'
SECRET_KEY = 'FSflP0+4RVhY9Zvv0Jen3wZ2OgeAbOuxBARAfHvj2oU'

REGION_NAME = 'sgp1'
ENDPOINT_URL = 'https://sgp1.digitaloceanspaces.com'

count_files = 0
all_files = {}
MAX_FILES = 30

currentdate = datetime.date.today()

# step 1 : Make a list of files to upload
files_to_upload = []

if len(sys.argv) > 1:
    for current_file in sys.argv[1:]:
        files_to_upload.append(current_file)
else:
    print("You have given No arguments")
    sys.exit(-1)

# step 2: Initiate session to upload file
session = session.Session()
client = session.client('s3',
                        region_name=REGION_NAME,
                        endpoint_url=ENDPOINT_URL,
			            aws_access_key_id=ACCESS_ID,
                        aws_secret_access_key=SECRET_KEY)

# Step 3: send notification to zulip
def send_notification(msg):
    """
    this method is used to send zulip chat notifications
    """
    data = {
        "type": "stream",
        "to": "MH_Associates",
        "subject": "EFF_Factor",
        "content": msg
    }
    try:
        return requests.post("https://fafadiatech.zulipchat.com/api/v1/messages", auth=HTTPBasicAuth("noreply@fafadiatech.com", "WRQl0wy0oga1rpwm65YIKoGjTW9qegGa"), data=data)
    except Exception as e:
        print(e)

# step 4: Upload a file to your Space
for current_file in files_to_upload:
    client.upload_file('{}'.format(current_file), 'efffactor-db-backup', '{}'.format(current_file))
    msg = '{} is UPLOADED on Digital Ocean Object Space'.format(current_file)
    send_notification(msg)
    print(current_file, "is uploaded")
print("****************************")

# step 5: get resource
resource = session.resource("s3",
		              region_name=REGION_NAME,
                       endpoint_url=ENDPOINT_URL,
                       aws_access_key_id=ACCESS_ID,
                       aws_secret_access_key=SECRET_KEY)


# Step 6 : Count the files on DO Space 
for key in client.list_objects(Bucket='efffactor-db-backup')['Contents']:
    if key['Key']:
        count_files += 1
        file_name = key['Key']
        file_uploaded_date_time = key['LastModified']
        file_uploded_date = file_uploaded_date_time.date()
        all_files[file_name] = file_uploded_date
        print(file_name,file_uploded_date)
print('--------------------')


# Step 7 : get files to delete
def get_files_to_delete(all_files):
    files_to_delete = []

    file_index = 0
    for item in sorted(all_files, key=all_files.get, reverse=True):
        if file_index >= MAX_FILES:
            files_to_delete.append(item)
        file_index += 1
    return files_to_delete


# Step 8: print the file and the delete the file
if count_files > MAX_FILES:
    files = get_files_to_delete(all_files)
    print('###########################')
    for current_file in files:
        print(current_file)
        obj = resource.Object('efffactor-db-backup', '{}'.format(current_file))
        obj.delete()
        msg = "{} is DELETED from Digital Ocean Object Space ".format(current_file)
        send_notification(msg)
