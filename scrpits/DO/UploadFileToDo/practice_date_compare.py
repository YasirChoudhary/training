import datetime  
# date in yyyy/mm/dd format 
d1 = datetime.date(2018, 9, 26) 
d2 = datetime.date(2018, 6, 1) 
  
# Comparing the dates will return 
# either True or False 
print("d1 is greater than d2 : ", d1 > d2) 
print("d1 is less than d2 : ", d1 < d2) 
print("d1 is not equal to d2 : ", d1 != d2) 
