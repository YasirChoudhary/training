from datetime import datetime, timedelta

all_files = {}
today = datetime.now().date()
N = 32
MAX_FILES = 10
for i in range(1, N):
    all_files[str(N - i)] = today - timedelta(days=i)

def get_files_to_delete(all_files):
    files_to_delete = []

    file_index = 0
    for item in sorted(all_files, key=all_files.get, reverse=True):
        if file_index >= MAX_FILES:
            files_to_delete.append(item)
        file_index += 1
    return files_to_delete

results = get_files_to_delete(all_files)
print(results)