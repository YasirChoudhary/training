### To deploy efffactor backup script on server
1. Copy script and requirement file to the server `scp files_to_upload_on_do.py requirements.txt ubuntu@159.65.149.149:/home/ubuntu/`
1. SSH to the the server `ssh ubuntu@159.65.149.149`
1. Create virtual environment `virtualenv -p /usr/bin/python3 ~/Installs/backup`
1. Activate virtual environment `source ~/Installs/backup/bin/activate`
1. Install requirements `pip install -r requirements.txt`
1. Copy files to the postgresql directory `sudo cp files_to_upload_on_do.py /var/lib/postgresql/`
1. Change the owner of a file `sudo chown postgres:postgres /var/lib/postgresql/files_to_upload_on_do.py`
1. Edit `db_backup.sh` file
    1. add following line on line number 12
`/home/ubuntu/Installs/backup/bin/python files_to_upload_on_do.py $file`
save and close



