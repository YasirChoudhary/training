from boto3 import session
from botocore.client import Config
import datetime
import sys

ACCESS_ID = 'LBFDKVGDEKTALQR2JBUJ'
SECRET_KEY = 'FSflP0+4RVhY9Zvv0Jen3wZ2OgeAbOuxBARAfHvj2oU'

REGION_NAME = 'sgp1'
ENDPOINT_URL = 'https://sgp1.digitaloceanspaces.com'

count_files = 0
file_to_delete = []

currentdate = datetime.date.today()

# step 1 : Make a list of files to upload
files_to_upload = []

if len(sys.argv) > 1:
    for current_file in sys.argv[1:]:
        files_to_upload.append(current_file)
else:
    print("You have given No arguments")
    sys.exit(-1)

# step 2: Initiate session to upload file
session = session.Session()
client = session.client('s3',
                        region_name=REGION_NAME,
                        endpoint_url=ENDPOINT_URL,
			            aws_access_key_id=ACCESS_ID,
                        aws_secret_access_key=SECRET_KEY)

# step 3: Upload a file to your Space
for current_file in files_to_upload:
    client.upload_file('{}'.format(current_file), 'efffactor-db-backup', '{}'.format(current_file))
    print(current_file, "is uploaded")

# step 4: get resource
resource = session.resource("s3",
		              region_name=REGION_NAME,
                       endpoint_url=ENDPOINT_URL,
                       aws_access_key_id=ACCESS_ID,
                       aws_secret_access_key=SECRET_KEY)


# Step 5 : Count the files on DO Space 
for key in client.list_objects(Bucket='efffactor-db-backup')['Contents']:
    if key['Key']:
        count_files += 1

# Step 6 : Delete files which are on the DO space more than 30 days
if count_files > 30 :
    for key in client.list_objects(Bucket='efffactor-db-backup')['Contents']:
        print(key['Key'], key['LastModified'])
        file_name = key['Key']
        file_uploaded_date_time = key['LastModified']
        file_uploded_date = file_uploaded_date_time.date()
        date_difference = currentdate - file_uploded_date
        days_difference = date_difference.days
        if days_difference > 30:
            if count_files == 30:
                break
            else:
                file_to_delete.append(file_name)
                obj = resource.Object('efffactor-db-backup', '{}'.format(file_name))
                obj.delete()
                print(file_name,' is deleted')
                count_files -= 1
        else:
            print(file_name, "not older than one Month")

print("Executed")
