file_list = ["88961286.csv", "101737053.csv", "114194578.csv", "127511417.csv", "140857889.csv", "152491759.csv", "163018632.csv", "177913155.csv", "191654856.csv", "205804909.csv", "220527453.csv", "234284478.csv", "249309576.csv", "277704233.csv", "292718216.csv"]

find_words = ["Hep", "eafile", "sms-portal", "fafadia", "YMHTDryRun", "tasr", "zeus", \
"yellowslice", "midas", "new", "xpertconnect", "ICICI", "polymath", "bhagwaanji", "Croma", \
"SR-Rooms", "sentry-server", "jenkins", "practiceagile", "biglife", \
"Odoo-2", "ada", "issues", "efffactor", "rince", "coloany", "eprecision", "lintell", "kindhearted", \
"dev-kh-eff", "New-Beep-Image", "SuperSales-Image", "accurate-dms-demo", "odoo-payroll", "haptik-dev", \
"loanxpress", "fairbridge", "New-Dev-Eff", "ft-demos", "crawl"]

party = ""
file_to_save = open("merge_all_files.csv", "w")
file_to_save.write("Party,Invoice No, Instance Details,hours,From Date,To Date,Amount\n")

'''
processed = []

for word in find_words:
    if word not in processed:
        processed.append(word)
    else:
        print("No duplicates found")
print("Processing completed")
'''

'''
for items in file_list:
    items = open(items).readlines()
    for word in find_words:
        for row in items:
            if word in row:
                print(row)
'''

count = 0
total_amount = 00.00
for word in find_words:
    for items in file_list:
        invoice_no = items.replace(".csv", "")
        ind_item = open(items).readlines()
       
        for row in ind_item:
            
            row = row.strip()
            descp, hours, start, end, amount = row.split(",")
            if amount== "USD":
                continue
            amount = float(amount.strip())
            if word in descp:
                count += 1
                total_amount += amount
                file_to_save.write("{}, {}, {}, {}, {}, {}, {} \n".format(party,invoice_no,descp,hours,start,end,amount))

    file_to_save.write("\n")
    file_to_save.write("\n")
    file_to_save.write("\n")

         
file_to_save.close()
print("Rows Processed : ", count)
print("Total Amount: ", total_amount)
print("Processing completed")