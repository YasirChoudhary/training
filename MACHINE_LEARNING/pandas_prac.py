
# coding: utf-8

# In[2]:


import pandas as pd


# In[3]:


file_path = '/home/oankar/YASIR/MACHINE_LEARNING/sample.csv'

emp_data = pd.read_csv(file_path)

print(emp_data)


# In[4]:


print(emp_data.describe())


# In[8]:


print(emp_data.columns)


# # Selecting and filtering Data

# In[11]:


file_path = '/home/oankar/YASIR/MACHINE_LEARNING/sample.csv'
emp_data = pd.read_csv(file_path)
# Selecting a single column
emp_data_name = emp_data.Name
# The head command returns top few lines of data if we have large number of rows
print(emp_data_name.head())


# ### dropna Example

# In[13]:


file_path = '/home/oankar/YASIR/MACHINE_LEARNING/sample.csv'
emp_data = pd.read_csv(file_path)
# dropna removes misssing values 0 for rows and 1 for columns
emp_data = emp_data.dropna(axis=0) # It removes the row which contain missing values
print(emp_data)

