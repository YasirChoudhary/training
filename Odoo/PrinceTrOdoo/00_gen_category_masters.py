
root_category_lookup = {}

with open("02-SubCategories-Exported.csv") as feed:
    for row in feed.readlines():
        row = row.strip()
        cat_id, name, parent_id = row.split(",")
        name = name.replace("\"", "")
        cat_id = cat_id.replace("\"", "")
        root_category_lookup[name] = cat_id

'''
file_to_save = open("04-Level-1-Master.csv", "w")
file_to_save.write("name,parent_id/id\n")
level_1_processed = []
with open("03-Category-Hierachy.tsv") as feed:
    for row in feed.readlines():
        row = row.strip()
        row = row.split("\t")
        
        if (len(row) < 2):
            continue

        brand, sub_category = row[0], row[1]
        rec = "%s,%s\n" % (sub_category, root_category_lookup[brand])

        if rec not in level_1_processed:
            file_to_save.write(rec)
            level_1_processed.append(rec)

print("Level 1 file Created: 04-Level-1-Master.csv")
'''

level_two = []
root_category_lookup_inverted = {}
level_two_mapping = {}


for k, v in root_category_lookup.items():
    root_category_lookup_inverted[v] = k
    print(root_category_lookup_inverted[v])
    

with open("03-Category-Hierachy.tsv") as feed:
    for row in feed.readlines():
        row = row.strip()
        if row not in level_two:
            if(len(row.split("\t")) >= 3):
                row = row.split("\t")[:3]
                level_two.append("\t".join(row))

with open("05-Level-1-Master-Exported.csv") as feed:
    for row in feed.readlines():
        row = row.strip()
        current_id, name, parent_id = row.split(",")

        current_id = current_id.replace("\"", "")
        name = name.replace("\"", "")
        parent_id = parent_id.replace("\"", "")

        if parent_id.strip() != "" and parent_id.find("parent_id/id") == -1 and name.find("Saleable") == -1:
            parent_category_friendly = root_category_lookup_inverted[parent_id]

            if parent_category_friendly not in level_two_mapping:
                level_two_mapping[parent_category_friendly] = {}
            
            level_two_mapping[parent_category_friendly][name] = current_id

'''
file_to_save = open("06-Level-2-Master.csv", "w")
file_to_save.write("name,parent_id/id\n")
level_2_processed = []
for current in level_two:
    brand, category, current = current.split("\t")
    rec = "%s,%s\n" % (current, level_two_mapping[brand][category])
    if rec not in level_2_processed:
        file_to_save.write(rec)
        level_2_processed.append(rec)

print("Level 2 file Created: 06-Level-2-Master.csv")
'''


level_3 = []
level_two_inverted = {}
level_three_mapping = {}

with open("03-Category-Hierachy.tsv") as feed:
    for row in feed.readlines():
        row = row.strip()
        if row not in level_3:
            if(len(row.split("\t")) == 4):
                level_3.append(row)

for brand in level_two_mapping:
    for current_name, current_id in level_two_mapping[brand].items():
        level_two_inverted[current_id] = "%s\t%s" % (brand, current_name)


with open("07-Level-2-Master-Exported.csv") as feed:
    for row in feed.readlines():
        row = row.strip()
        current_id, name, parent_id = row.split(",")

        current_id = current_id.replace("\"", "")
        name = name.replace("\"", "")
        parent_id = parent_id.replace("\"", "")

        if parent_id in level_two_inverted:
            brand, category = level_two_inverted[parent_id].split("\t")
            
            if brand not in level_three_mapping:
                level_three_mapping[brand] = {}
            
            if category not in level_three_mapping[brand]:
                level_three_mapping[brand][category] = {}
            
            level_three_mapping[brand][category][name] = current_id

file_to_save = open("08-Level-3-Master.csv", "w")
file_to_save.write("name,parent_id/id\n")
level_3_processed = []
for current in level_3:
    a, b, c, d = current.split("\t")
    rec = "%s,%s\n" % (d, level_three_mapping[a][b][c])
    if rec not in level_3_processed:
        file_to_save.write(rec)
        level_3_processed.append(rec)

print("Level 3 file Created: 08-Level-3-Master.csv")
print(level_three_mapping)


