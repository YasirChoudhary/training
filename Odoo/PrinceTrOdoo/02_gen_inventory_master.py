import xlrd
import pprint

pp = pprint.PrettyPrinter(indent=4)
workbook = xlrd.open_workbook("InventoryMaster.xlsx")
worksheet = workbook.sheet_by_name('Odoo Stock-Dubai (5)')
total_rows = worksheet.nrows

mappings = {"Dubai Main": "__export__.stock_location_18", "Tecbuy": "__export__.stock_location_19", "Sharaf DG": "__export__.stock_location_19", "Tarsam": "__export__.stock_location_21", "Istyle DFC": "__export__.stock_location_22", "I Store Sharjah City Centre": "__export__.stock_location_23"}

print("Rows in Inventory Master file:", total_rows)

inventory = {}
product_ids = {}

def dump_csv(location_name, data):
    file_name = "%s.csv" % location_name
    file_to_save = open(file_name, "w")
    file_to_save.write("name,line_ids/location_id/id,line_ids/product_id/id,line_ids/product_qty\n")
    for i, row in enumerate(data):
        if i == 0:
            name = "Starting Stock for %s" % location_name
            rec = ",".join([name, row[0], row[1], row[2]]) + "\n"
        else:
            name = ""
            rec = ",".join([name, row[0], row[1], row[2]]) + "\n"
        file_to_save.write(rec)

with open("14-Product-Lookup-Exported.csv") as feed:
    for row in feed.readlines():
        row = row.strip()
        current_id, internal_ref = row.split(",")
        current_id = current_id.replace("\"", "")
        internal_ref = internal_ref.replace("\"", "")
        product_ids[internal_ref] = current_id

for i in range(1, total_rows):
    current_row = worksheet.row(i)
    internal_ref = str(current_row[4].value).replace(".0", "").strip()
    quantity = str(current_row[7].value).strip().replace(".0", "")
    location = str(current_row[10].value).strip()

    if location.strip() == "":
        continue

    if location not in inventory:
        inventory[location] = []
    
    if internal_ref not in product_ids:
        print("Not found:", internal_ref)
        continue

    inventory[location].append((mappings[location], product_ids[internal_ref], quantity))

for location, data in inventory.items():
    dump_csv(location, data)

print("Created inventories files:", len(inventory))
