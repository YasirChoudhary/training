import xlrd
import pprint

pp = pprint.PrettyPrinter(indent=4)
workbook = xlrd.open_workbook("InventoryMaster.xlsx")
worksheet = workbook.sheet_by_name('Odoo Stock-Dubai (5)')
total_rows = worksheet.nrows

print("Rows in Inventory Master file:", total_rows)




file_to_save = open("CostPrice.csv", "w")
#file_to_save.write("default_code,standard_price\n")


for i in range(1, total_rows):
    current_row = worksheet.row(i)
    internal_ref = str(current_row[4].value).strip()
    standard_price = str(current_row[8].value).strip()

    #key = "\t".join([internal_ref, standard_price])
    rec = "{}, {}\n".format(internal_ref,standard_price)
    file_to_save.write(rec)

print("Cost price file is created")
 





