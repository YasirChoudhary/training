
journal entries csv field (account.move.csv):
	- id : It will automatically generated we don't require this field
	- journal_id/id : 
	- line_id/name :
	- line_id/partner_id/id :
	- line_id/account_id/id :
	- line_id/debit :
	- line_id/credit :



Step 1:
- For adding journal entries we require journal name.

- We can fetch the journal name by its journal_id/id using the (account.journal.csv) (journal) file
- We can get journal_id/id by exporting "journal"

- name is mapped by id field in (account.move.csv) (journal entries)

Step 2:
- For adding journal items in journal entries we require
  - Name
  - Partner
  - Account
  - Debit 
  - Credit

	- Name: line_id/name
	- Partner: for adding journal entries we require partner_id we can get line_id/partner_id/id by exporting "Customers" 
		 	Customers --> (res.partner.csv)

        - Account: for adding journal entries we require account_id we can get line_id/account_id/id by exporting "Accounts"
			Accounts --> (account.account.csv)
        - Debit: line_id/debit 
	- Credit: line_id/credit





