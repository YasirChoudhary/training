cost_file = open("CostPrice.csv").readlines()
prod_file = open("11-Product-Master-Cost-Price.csv").readlines()


file_to_save = open("12-Merge-Cost-Price.csv", "w")
file_to_save.write("id,name,standard_price,default_code\n")

default_code = "1"

for row in cost_file:
    row = row.strip()
    internal_ref, standard_price = row.split(",")
    internal_ref = str(internal_ref).replace(".0","").replace("\"", "").replace("['", "").replace("']","").strip()
    standard_price = str(standard_price).replace("\"", "").replace("['", "").replace("']","").strip()

    for row in prod_file:
        
        row = row.strip()
        c_id, name, default_code = row.split(",")
        c_id = str(c_id).replace("\"", "").replace("['", "").replace("']","").strip()
        name = str(name).replace("\"", "").replace("['", "").replace("']","").strip()
        default_code = str(default_code).replace(".0","").replace("\"", "").replace("['", "").replace("']","").strip()

        if internal_ref == default_code:
            rec = "{}, {}, {}, {}\n".format(c_id,name,standard_price,internal_ref)
            file_to_save.write(rec)
            break
        
    if internal_ref != default_code:
        print("Not Found ",internal_ref)

print("Cost price Merged")
file_to_save.close()      


