import xlrd
import pprint

pp = pprint.PrettyPrinter(indent=4)
workbook = xlrd.open_workbook("InventoryMaster.xlsx")
worksheet = workbook.sheet_by_name('Odoo Stock-Dubai (5)')
total_rows = worksheet.nrows

print("Rows in Inventory Master file:", total_rows)

lookup = {}
parent = {}

with open("09-Final-Product-Categories-Master.csv") as feed:
    for row in feed.readlines():
        row = row.strip()
        current_id, name, parent_id = row.split(",")

        current_id = current_id.replace("\"", "")
        name = name.replace("\"", "")
        parent_id = parent_id.replace("\"", "")
        
        if current_id not in lookup:
            lookup[current_id] = name
        
        if current_id not in parent:
            parent[current_id] = parent_id

mappings = {}

def find_path(node_id):
    path = [node_id]
    current_id = node_id
    while current_id in parent:
        if parent[current_id] != "":
            path.append(parent[current_id])
        current_id = parent[current_id]
    path.reverse()
    return path

def friendly_path(path):
    result = []
    for item in path:
        result.append(lookup[item])
    return result

for current_node in lookup:
    path = find_path(current_node)
    if len(path) >= 3:
        key = "\t".join(friendly_path(path))
        mappings[key] = current_node

file_to_save = open("10-Product-Master-Consolidated.csv", "w")
file_to_save.write("name,categ_id/id,product_variant_ids/default_code\n")
# pp.pprint(mappings)

not_found = 0
total_found = 0
internal_ref_mapping = {}

for i in range(1, total_rows):
    current_row = worksheet.row(i)
    brand = str(current_row[0].value).strip()
    level1 = str(current_row[1].value).strip()
    level2 = str(current_row[2].value).strip()
    level3 = str(current_row[3].value).strip()

    if level3 != "":
        key = "\t".join([brand, level1, level2, level3])
    else:
        key = "\t".join([brand, level1, level2])

    internal_ref = str(current_row[4].value).replace(".0", "").strip()
    name = str(current_row[5].value).strip()
    name = name.replace(",", " ")

    if key not in mappings:
        not_found += 1
        print("Cannot Process:", internal_ref)
        continue
    
    if internal_ref not in internal_ref_mapping:
        internal_ref_mapping[internal_ref] = mappings[key]
        rec = "%s,%s,%s\n" % (name, mappings[key], internal_ref)
        file_to_save.write(rec)

    total_found += 1

print("Product Master Created: 10-Product-Master-Consolidated.csv")