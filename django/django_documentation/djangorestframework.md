## djangorestframework
### Serializing django objects
 Django’s serialization framework provides a mechanism for “translating” Django models into other formats. Usually these other formats will be text-based and used for sending Django data over a wire, but it’s possible for a serializer to handle any format (text-based or not)

### Viewsets
Rather than write multiple views we're grouping together all the common behavior into classes called ViewSets 
```
from rest_framework import viewsets
(viewsets.ModelViewSet)
```

### Pagination
Pagination allows you to control how many objects per page are returned. To enable it add following lines:
```
REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10
}
```
### Serializer or ModelSerializer
It would be nice if we could keep our code a bit more concise.
In the same way that Django provides both **Form** classes and **ModelForm** classes, REST framework includes both **Serializer** classes, and **ModelSerializer** classes.

It's important to remember that ModelSerializer classes don't do anything particularly magical, they are simply a shortcut for creating serializer classes:
1. An automatically determined set of fields.
2. Simple default implementations for the create() and update() methods.

### Note: CRUD Operation

| Operation | HTTP method |	endpoint type |
| :-------- | :---------  | :-----------  |
| Create	| POST	      | list          |
| Retrieve many | GET	| list |
| Retrieve one	| GET	| detail |
| Update	| PUT / PATCH | detail |
| Delete | DELETE |	detail |
