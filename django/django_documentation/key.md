### Primary Key 

when we create a models django by default create a primary key.
django generate unique id for each record while inserting record in the table start from "1".

### Foreign Key
```python
class Class1(models.Model):
    field1 = models.CharField(max_length=100)
class Class2(models.Model):
    class1 = models.ForeignKey(Class1, on_delete=models.CASCADE)
    fieldname = modles.CharField(max_length)
```
When we delete any record from Class1 table(or model) the Class2 record associated with the  Class1 record is also deleted, because we have used a foreign key Concept
"on_delete=models.CASCADE".

