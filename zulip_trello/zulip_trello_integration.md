## Zulip <-> Trello Integration
1. Create the stream you'd like to use for Trello notifications.
2. Create topic in that stream.
3. Construct the URL for the Trello bot using the bot's API key and the desired stream name:
```sh
https://fafadiatech.zulipchat.com/api/v1/external/trello?api_key=WRQl0wy0oga1rpwm65YIKoGjTW9qegGa&stream=MH_Associates&topic=ft_training
```

### Log in to Trello, and collect the following three items:
##### Board ID: Go to your Trello board. The URL should look like
```https://trello.com/b/<BOARD_ID>/<BOARD_NAME>. Note down the  <BOARD_ID>.```
API Key: Go to ```https://trello.com/1/appkey/generate```. Note down the key listed under Developer API Keys.
User Token: Go to ```https://trello.com/1/appkey/generate```. Under Developer API Keys, click on the Token link. Click on Allow. Note down the token generated.

###### You're now going to need to run a Trello configuration script.

###
##### Run the zulip-trello script
1. you need to put TRELLO_API_KEY, TRELLO_TOKEN, ZULIP_WEBHOOK_URL in zulip_trello_config.py

2. Run ```python zulip_trello.py --trello-board-name <trello_board_name> \ --trello-board-id   <trello_board_id>```

