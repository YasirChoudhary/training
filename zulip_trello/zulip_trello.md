## Trello

### Install Zulip
```sh
pip install zulip
```

### Trello directory
```
/home/yasir/.local/share/zulip/integrations/trello
```

### Trello Webhook url
```sh
https://fafadiatech.zulipchat.com/api/v1/external/trello??api_key=w5bCQfdkK6cZ7cwstgSuVEIk0m1NnwXO&stream=trello&topic=trello_cards
```

### Trello API Key
```sh
Trello APIKey = 3c4eace5dd773152d2fba7b374338a5c
```

### Trello Board ID
```sh
BoardID = iLnYyR9r
```

### Trello User Token
```sh
Trello UserToken = 481207b32e924a945bec3ef1379d8ee943dd1fd6221bb1bd83279944be70ce2d
```
