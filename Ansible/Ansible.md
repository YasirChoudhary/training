# Ansible 
****
##### What is  Ansible ?
1. Ansible is a configuration Management, Deployment & Orchestration tool.
1. It is “push-based” configuration management tool.
1. It automates your entire IT infrastructure by providing large productivity gains.

##### Configuration Management
1. If any of the server fails we can reconfigure using configuration management.
1. We can also replace the server if needed.
1. Using configuration we can easily scale up our servers.
1. we can easily distribute traffic using configuration management.
1. we can easily downgrade our version if user’s doesn’t like new vesion.
1. Establishes and maintains consistency of a product’s performance.
1. Maintains physical attributes with its requirements and design.
1. Preseves operational information throughout it’s lifecycle.

##### What is snapshot?
It records the configuration details which is nothing but datastored.
**_cmdb_** : Configuration Management Database
Here all the snapshot will be stored.

##### Configuration Management tools
###### pull-based configuration management tools
1. puppet
1. cheff

###### push-based configuration management tools
1. SALTSTACK
1. Ansible

## Ansible language Basics
Playbooks contains plays
plays contains tasks
Tasks call modules

Tasks run sequentially

Handlers are triggered by tasks and run once at the end of plays.

**Security**
Define security with playbooks:
We can setup firewalls using playbooks.

**Ansible Control Machine**
Perform below commands on control machine to install ansible
```sh
$ sudo apt-get update
$ sudo apt-get install software-properties-common
$ sudo apt-add-repository ppa:ansible/ansible
$ sudo apt-get update
$ sudo apt-get install ansible
```

To check the version of ansible
```sh
$ ansible --version
```

Create a ssh key
```sh
$ ssh-keygen
```

Copy the ssh public id to the host machine.
```sh
$ ssh-copy-id -i yasir@10.0.2.15
```

Edit the **hosts** file and add group name and ip addresses
```sh
$ sudo nano /etc/ansible/hosts
[test_servers]
UserName@10.0.2.15
```
where **[test_servers]** is the group name.

To test the server connection with the host:
```sh
$ ansible -m ping 'test_servers'
```

Write a playbook using yaml file
```sh
$ nano nginx.yml
```
```sh
---
- hosts : test_servers
  become : true
  become_user : root
  gather_facts : true
  tasks :
   - name : install nginx
     apt : pkg=nginx state=present

     notify:
      - start nginx

  handlers :
   - name : start nginx
     service : name=nginx state=started
```

**notify:** To strat nginx we have to set a trigger for **handler**. Here we have to metion the name of the handler.

**Run playbook**
```sh
$ ansible-playbook nginx.yml
```

**Ansible Host(Server) Machine**
To check nginx is installed or not on host machine.
```sh
$ ps waux | grep nginx
```
****
### To clone the git repository see below playbook
```sh
$ nano git_clone.yml
```

```sh
---
- hosts: test_servers
  become : true
  become_user : yasir
  gather_facts : true
  
  tasks :
   - name : create a directory
     file : path=/home/yasir/Code state=directory

   - name : Install git
     apt : name=git state=present
     become : true
     become_user : yasir

   - name : Clone_a_repo
     git : repo=git@bitbucket.org:fafadiatech/lintell-solutions.git dest=/home/yasir/Code
     
```

To run the playbook type below command
```sh
$ ansible-playbook git_clone.yml
```
If it asks for sudo password run with below command
```sh
$ ansible-playbook git_clone.yml -K
```
If it asks for ssh password run with below command
```sh
$ ansible-playbook git_clone.yml -k
```
If it asks for ssh and sudo password run with below command
```sh
$ ansible-playbook git_clone.yml -k -K
```

****
### To install django on host(Server) see below playbook
```sh
$ nano installdjango.yml
```
```sh
---
- hosts : test_servers
  become : true
  become_user : yasir
  gather_facts : true
  vars:
   PROJECT_HOME: "/home/yasir/Install"
  
  tasks :
   - name : creates a directory
     file : path=/home/yasir/Install state=directory
    
   - name : Install "pip"
     apt : name=python-pip state=present
     become : true
     become_user : root

   - name : Install virtualenv
     pip : name=virtualenv
     become : true
     become_user : root
      
   - name: Create virtualenv for project
     shell: virtualenv "{{ PROJECT_HOME }}/test"
            creates="{{ PROJECT_HOME }}/test/bin/activate" 

   - name : Install django
     pip : name=django virtualenv="{{ PROJECT_HOME }}/test"

```
To run the playbook type below command

```sh
$ ansible-playbook installdjango.yml
```

****
### To install requirement.txt on host(Server) machine
```sh
$ nano install_requirement.yml
```
```sh
---
- hosts : test_servers
  become : true
  become_user : yasir
  gather_facts : true
  vars:
   PROJECT_HOME: "/home/yasir/Install"
  
  tasks :
   - name : creates a directory
     file : path=/home/yasir/Install state=directory
    
   - name : Install "pip"
     apt : name=python-pip state=present
     become : true
     become_user : yasir

   - name : Install virtualenv
     pip : name=virtualenv
     become : true
     become_user : yasir
      
   - name: Create virtualenv for project
     shell: virtualenv "{{ PROJECT_HOME }}/test"
            creates="{{ PROJECT_HOME }}/test/bin/activate" 

   - name : Install Project requirements
     pip : 
      requirements: /home/yasir/Install/test/requirements.txt 
      virtualenv : "{{ PROJECT_HOME }}/test" 
     become : true
     become_user : yasir
```
To run the playbook type below command
```sh
$ ansible-playbook install_requirement.yml
```
****
### NOTE:
At the time of running the playbook if it asks for sudo password run with below command
```sh
$ ansible-playbook plabook_name.yml -K
```
At the time of running the playbook if it asks for ssh password run with below command
```sh
$ ansible-playbook playbook_name.yml -k
```
At the time of running the playbook if it asks for ssh and sudo password run with below command
```sh
$ ansible-playbook git_clone.yml -k -K
```









